<?php

use App\User;
use Carbon\Carbon;
use App\Mail\JaboEmails;
use App\Notifications\GeneralNotification;
use Edujugon\PushNotification\PushNotification;
use Facades\App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::view('test', 'uploadFile');
/*
|---------------------------------------------------------------------------------------
| IMAGES ROUTES
|---------------------------------------------------------------------------------------
*/

Route::get('resources/{image}', 'ImageController@getImage');
Route::get('resources/activities/{image}/{size?}/{status?}', 'ImageController@getActivitiesIcons');

/*
|---------------------------------------------------------------------------------------
| USER ROUTES
|---------------------------------------------------------------------------------------
*/
	
Route::get('user/login', 'AccountController@login');
Route::get('user/delete_account', 'AccountController@delete');
Route::get('user/logout', 'AccountController@logout');


Route::get('user/info', 'UserController@getProfile');

Route::get('user/edit_profile', 'UserController@editProfile');
Route::get('user/profile/edit', 'UserController@editProfile');

Route::get('user/set_idle', 'UserController@setIdle');
Route::get('user/add_device_token', 'UserController@addDeviceToken');

Route::get('user/validate_location_waitlist', 'LocationController@ValidateUserWaitlistByLocation');
Route::get('user/location/validate_waitlist', 'LocationController@ValidateUserWaitlistByLocation');

Route::get('user/send_feedback', 'FeedbackController@sendFeedback');
Route::get('user/view_profile', 'UserController@viewProfile');

Route::get('user/set_health_info', 'HealthInfoController@setHealthInfo');
Route::get('user/health/set_info', 'HealthInfoController@setHealthInfo');

Route::get('user/get_health_info', 'HealthInfoController@getHealthInfo');
Route::get('user/health/get_info', 'HealthInfoController@getHealthInfo');

Route::get('user/notifications/count_pending_notifications', 'NotificationController@countPendingNotifications');
Route::get('user/total_pending_notifications', 'NotificationController@countPendingNotifications'); // Old Url


Route::get('user/notifications/mark_notification_as_viewed', 'NotificationController@markNotificationAsViewed');
Route::get('user/mark_notification_as_viewed', 'NotificationController@markNotificationAsViewed');

Route::get('user/notifications/mark_notification_as_viewed_by_sender_id', 'NotificationController@MarkNotificationAsViewedBySenderId');
Route::get('user/mark_notification_as_viewed_by_sender_id', 'NotificationController@MarkNotificationAsViewedBySenderId');

Route::get('user/notifications/pending_notifications', 'NotificationController@getPendingNotification');
Route::get('user/pending_notifications', 'NotificationController@getPendingNotification');

Route::get('notifications/notificacion_received', 'NotificationController@storeNotificacionReceived');
Route::get('user/notificacion_received', 'NotificationController@storeNotificacionReceived');

Route::post('user/profile/upload_picture', 'UserController@UploadPicture');
Route::get('user/upload_profile_picture', 'UserController@UploadPicture');


Route::get('user/pics_from_gallery', 'ImageController@getProfilePictures');

Route::get('user/profile/delete_picture', 'UserController@DeletePicture');
Route::post('user/delete_profile_picture', 'FitdateGalleryPictureController@destroy');

Route::get('user/pictures/move', 'UserPictureController@movePicture');
Route::post('user/move_photo', 'FitdateGalleryPictureController@movePhoto');

Route::get('user/friends/set', 'FriendsController@setFriends');
Route::post('user/set_friends', 'FitdateFriendsController@set_friends');

