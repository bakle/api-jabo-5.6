<?php

return [
	'image_storage' => storage_path()."/app/images/",
	'image_cloud' => env('PATH_IMAGE_CLOUD', '/mnt/images/'),
	'image_default' => env('PATH_IMAGE_DEFAULT', '/mnt/images/default/'),
	'image_icons' => storage_path() . "/app/images/icons/activities/",
];