<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => env('ANDROID_PUSH_NOTIFICATION_API_KEY'),
  ],
  'apn' => [
      'certificate' => storage_path() . '/app/ios-push-certificate/jabo_apns_28052018.pem',
      'passPhrase' => '', //Optional
      'passFile' => '', //Optional
      'dry_run' => false
  ]
];