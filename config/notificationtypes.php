<?php

return [
    'new_chat' => 1,
    'new_match' => 2,
    'activity_request_received' => 3,
    'new_suggestion' => 4,
    'activity_changed' => 5,
    'activity_request_accepted' => 6,
    'activity_request_rejected' => 7,
    'activity_request_received_match_pass_sender' => 8,
    'new_suggestion_match_pass_sender' => 9,
    'activity_request_rejected_match_pass_sender' => 10,
    'new_match_match_pass_sender' => 11,
    'match_update_app' => 12,
    'jump_to_chat' => 13,
    'matchpass_to_expire' => 14,
    'new_matchpass' => 15,
    'users_liked_you' => 16,
];