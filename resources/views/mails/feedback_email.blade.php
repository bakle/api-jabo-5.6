@component('mail::message')

<strong>Subject</strong> <br>
{{ $data->text }}

<br>

<div style="width:100%; text-align: center;">
    <img src="{{ optional($user->pictures->first())->image_url }}" width="40%" style="border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;" alt=""> <br>
    {{ $user->full_name }}
</div>

<br>

<strong>User Info</strong>

@component('mail::table')
    |        |        |
    | ------ | ------ |
    | **User Id** | {{ $user->user_id }} |
    | **Facebook Id** | {{ $user->facebook_id }} |
    | **Location** | {{ optional($user->location->first())->zone }}, {{ optional($user->location->first())->state }} |
    | **Age** | {{ $user->birthday->age }} |
    | **Gender** | {{ $user->gender_name }} |
@endcomponent


<strong>Discovery Info</strong>

@component('mail::table')
    |        |        |
    | ------ | ------ |
    | **Discovery** | {{ $user->discovery_setting_status }} |
    | **Distance** | {{ $user->distance_setting }} |
    | **Age** | {{ $user->age_setting }} |
    | **Show** | {{ $user->show_gender_setting }} |
    | **Total Activities** | {{ $user->activity_types_count }} |
    | **Total Matches** | {{ $user->matches_count }} |
@endcomponent

<strong>Device Info</strong>

@component('mail::table')
    |        |        |
    | ------ | ------ |
    | **S.O** | {{ $data->so_platform }} |
    | **S.O Version** | {{ $data->so_version }} |
    | **Device Model** | {{ $data->device_model_name }} |
    | **App Version** | {{ $data->app_version }} |
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent