<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserInfoServiceTest extends TestCase
{
    /**
     * Testing Success Status
     *
     * @return void
     */
    public function testSuccessfully()
    {
        $response = $this->json('GET', 'api/user/info', [
            'user_id' => 880,
            'viewer_id' => 880,
        ]);

        $response
        ->assertStatus(200)
        ->assertJsonStructure([
            'status' => [
                'code',
                'message'
            ],
            'data' => [
                'user_id',
                'weight',
                'weight_value',
                'height',
                'height_value',
                'gender',
                'facebook_id',
                'birthday',
                'about_me',
                'total_views',
                'latitude',
                'longitude',
                'distance',
                'last_name',
                'first_name',
                'email',
                'gender_search',
                'created_at',
                'total_facebook_friends',
                'last_activity',
                'idle',
                'matchpass_count',
                'health_info',
                'pictures',
                'common_activities',
                'common_friends',
                'common_interests',
            ]
            
        ]);
    }

    /**
     * Testing Failed Operation Status
     *
     * @return void
     */
    public function testFailedOperation()
    {
        $response = $this->json('GET', 'api/user/info', [
            'user_id' => 880,
            'viewer_id' => 880999,
        ]);

        $response
        ->assertStatus(200)
        ->assertExactJson([
            'status' => [
                'code' => -1,
                'message' => 'Error: Failed Operation'
            ]
        ]);
    }


    /**
     * Testing Incorrect Parameters Status
     *
     * @return void
     */
    public function testIncorrectParameters()
    {
        $response = $this->json('GET', 'api/user/info', [            
            'viewer_id' => 880,
        ]);

        $response
        ->assertStatus(200)
        ->assertExactJson([
            'status' => [
                'code' => -2,
                'message' => 'Error: incorrect parameters'
            ]
        ]);
    }


    /**
     * Testing Result Not Found Status
     *
     * @return void
     */
    public function testResultNotFound()
    {
        $response = $this->json('GET', 'api/user/info', [            
            'user_id' => 880999,
            'viewer_id' => 880,            
        ]);

        $response
        ->assertStatus(200)
        ->assertExactJson([
            'status' => [
                'code' => 0,
                'message' => 'Results not found'
            ]
        ]);
    }


    /**
     * Testing User Blocked Status
     *
     * @return void
     */
    public function testUserBlocked()
    {
        $response = $this->json('GET', 'api/user/info', [            
            'user_id' => 53,
            'viewer_id' => 880,            
        ]);

        $response
        ->assertStatus(200)
        ->assertExactJson([
            'status' => [
                'code' => -4,
                'message' => 'Error: User blocked'
            ]
        ]);
    }
}
