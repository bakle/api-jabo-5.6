<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserEditProfileTest extends TestCase
{
    /**
     * Testing Success Status
     *
     * @return void
     */
    public function testSuccessfully()
    {
        $response = $this->json('GET', 'api/user/edit_profile', [
            'user_id' => 880,            
            'about_me' => 'Testing About',            
            'gender' => 2,            
        ]);

        $response
        ->assertStatus(200)
        ->assertExactJson([
            'status' => [
                'code' => 1,
                'message' => 'Successfully'
            ]            
        ]);

        $this->assertDatabaseHas(env('DB_API_PREFIX') . 'user', [
            'user_id' => 880,
            'about_me' => 'Testing About',
            'gender' => 2,
        ]);
    }


    /**
     * Testing Incorrect Parameters Status
     *
     * @return void
     */
    public function testIncorrectParameters()
    {
        $response = $this->json('GET', 'api/user/edit_profile', [            
            'viewer_id' => '',
        ]);

        $response
        ->assertStatus(200)
        ->assertExactJson([
            'status' => [
                'code' => -2,
                'message' => 'Error: incorrect parameters'
            ]
        ]);
    }
}
