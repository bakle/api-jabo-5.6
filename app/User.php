<?php

namespace App;

use App\Setting;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'user_id';

    /**
     * Define the dates that will be Carbon instances
     *
     * @var array
     **/
    protected $dates = [
        'last_login',
        'last_activity',
        'matchpass_updated_at',
        'birthday'
    ];

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "user";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | RELATIONSHIPS
    |---------------------------------------------------------------------------------------
    */


    /**
     * This relationship brings the user's settings.
     * 
     * @author Bashir A.
     *
     **/
    public function settings()
    {
        return $this->belongsToMany(Setting::class, env('DB_API_PREFIX') . 'setting_user', 'user_id', 'setting_id')->withPivot("value")->withTimestamps();
    }

    /**
     * This relationship brings the activities that has been sent to Me.
     * 
     * @author Bashir A.
     *
     **/
    public function activitiesToMe()
    {
        return $this->hasMany(Activity::class, 'match_user_id');
    }

    /**
     * This relationship brings the user's activities that has been sent to another user.
     * 
     * @author Bashir A.
     *
     **/
    public function activitiesFromMe()
    {
        return $this->hasMany(Activity::class,'user_id');
    }


    /**
     * This relationship brings the user's activities that has been set in the settings.
     * 
     * @author Bashir A.
     *
     **/
    public function activityTypes()
    {
        return $this->belongsToMany(ActivityType::class, env('DB_API_PREFIX') . "user_activity_type", 'user_id', 'activity_type_id');
    }

    public function customActivities()
    {
        return $this->belongsToMany(ActivityType::class, env('DB_API_PREFIX') . 'user_activity_list', 'user_id', 'activity_type_id');
    }

    /**
     * This relationship brings the users that has been blocked by me.
     * 
     * @author Bashir A.
     *
     **/
    public function blocked()
    {
        return $this->hasMany(UserBlocked::class, "user_facebook_id", 'facebook_id');
    }

    /**
     * This relationship brings the user's health info.
     * 
     * @author Bashir A.
     *
     **/
    public function healthData()
    {
        return $this->hasMany(HealthData::class, "user_id");
    }

    /**
     * This relationship brings the user's pictures.
     * 
     * @author Bashir A.
     *
     **/
    public function pictures()
    {
        return $this->hasMany(GalleryPicture::class, "owner_user_id");
    }

    /**
     * This relationship brings the user's interests.
     * 
     * @author Bashir A.
     *
     **/
    public function interests()
    {
        return $this->hasMany(Interest::class, "user_id");
    }

    /**
     * This relationship brings the user's friends.
     * 
     * @author Bashir A.
     *
     **/
    public function friends()
    {
        return $this->hasMany(Friend::class,"user_id", "facebook_id");
    }

    /**
     * This relationship brings the user's friends 2.
     * 
     * @author Bashir A.
     *
     **/
    public function friends_2()
    {
        return $this->hasMany(Friend2::class,"user_id", "user_id");
    }

    /**
     * This relationship brings all user's sessions.
     * 
     * @author Bashir A.
     *
     **/
    public function sessions()
    {
        return $this->belongsToMany(Device::class, 'sessions', 'user_id', 'device_id')->withPivot('session_status_id', 'last_activity')->withTimestamps();
    }

    /**
     * This relationship brings user's active sessions.
     * 
     * @author Bashir A.
     *
     **/
    public function activeSessions()
    {
        return $this->belongsToMany(Device::class, 'sessions', 'user_id', 'device_id')->wherePivot('deleted_at', '=', null)->withTimestamps();
    }

    public function swipesToMe()
    {
        return $this->hasMany(Swipe::class, 'user_id');
    }

    public function swipesFromMe()
    {
        return $this->hasMany(Swipe::class, 'viewer_id');
    }

    public function matches()
    {
        return $this->hasMany(Match::class, "user_id");
    }

    public function matchpassToMe()
    {
        return $this->hasMany(Matchpass::class, 'match_user_id');
    }

    public function matchpassFromMe()
    {
        return $this->hasMany(Matchpass::class,  'user_id');
    }

    public function chatMessagesFromMe()
    {
        return $this->hasMany(Chat::class, 'user_id');
    }

    public function chatMessagesToMe()
    {
        return $this->hasMany(Chat::class, 'match_id');
    }

    public function location()
    {
        return $this->belongsToMany(Location::class, env('DB_API_PREFIX') . 'user_location_validation', 'user_id', 'location_id')->withTimestamps();
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'viewer_user_id');
    }

    public function profilesViewed()
    {
        return $this->hasMany(ViewProfile::class, 'viewer_id');
    }

    public function reportDeleteAccount()
    {
        return $this->hasMany(ReportDeleteAccount::class, "user_id");
    }


    /*
    |---------------------------------------------------------------------------------------
    | METHODS
    |---------------------------------------------------------------------------------------
    */

    /**
     * This method verify if the users has been blocked by other user.
     * 
     * @author Bashir A.
     *
     **/
    public function isBlockedBy($viewer){
        $user_blocked = $this->blocked()->where("blocked_user_facebook_id", $viewer->facebook_id)->first();
        if($user_blocked){
            return true;
        }

        return false;
    }


    /*
    |---------------------------------------------------------------------------------------
    | ACCESSORS 
    |---------------------------------------------------------------------------------------
    */

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getGenderNameAttribute()
    {
        
        switch ($this->gender) {
            case 1:
                return 'Male';
            break;

            case 2:
                return 'Female';
            break;
            
            default:
                return 'No Gender';
            break;
        }
    }


    public function getDiscoverySettingStatusAttribute()
    {
        $value = optional(optional($this->settings->where('setting_id', 1)->first())->pivot)->value;

        return $value == 1 ? 'On' : 'Off';
    }


    public function getDistanceSettingAttribute()
    {
        $distance = optional(optional($this->settings->where('setting_id', 3)->first())->pivot)->value;
        $distance_in = optional(optional($this->settings->where('setting_id', 9)->first())->pivot)->value;

        if ($distance) {
            $distance = explode('-', $distance);
            $distance = $distance[1] . ' ' . $distance_in;
        }

        return $distance;
    }


    public function getAgeSettingAttribute()
    {
        $ages = optional(optional($this->settings->where('setting_id', 4)->first())->pivot)->value;

        if ($ages) {
            $ages = explode('-', $ages);
            $ages = $ages[1] . '-' . $ages[2];
        }

        return $ages;
    }


    public function getShowGenderSettingAttribute()
    {
        $show_gender = optional(optional($this->settings->where('setting_id', 2)->first())->pivot)->value;

       switch ($show_gender) {
           case 1:
               return 'Men';
            break;
           
            case 2:
               return 'Women';
            break;

            case 3:
               return 'Both';
            break;

           default:
               return 'No Gender To Search';
            break;
       }
    }

    
}
