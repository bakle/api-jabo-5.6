<?php

namespace App\Listeners;

use App\Events\RegisterDevice;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserSessionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * Register a new user session, if it exists, then updated it.
     *
     * @author Bashir A.
     * @param  RegisterDevice  $event
     * @return void
     */
    public function handle(RegisterDevice $event)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Get Any Active User Session
        |---------------------------------------------------------------------------------------
        */

        $active_session = $event->user->activeSessions()->wherePivot('device_id', $event->request->unique_device_id)->wherePivot('session_status_id', 1)->latest()->first();

        /*
        |---------------------------------------------------------------------------------------
        | If There Is Not A User Session Active, Then Create A New One.
        |---------------------------------------------------------------------------------------
        */

        if(!$active_session){
            if ($event->request->filled('unique_device_id')) {
                $event->user->sessions()->attach($event->request->unique_device_id, ['session_status_id' => 1, 'last_activity' => now()]);
            }
        }
        else {
            $active_session->pivot->update([
                'last_activity' => now()
            ]);
        }
    }
}
