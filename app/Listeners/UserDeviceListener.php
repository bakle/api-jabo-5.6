<?php

namespace App\Listeners;

use App\Device;
use App\Events\RegisterDevice;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserDeviceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * Register a new user device
     *
     * @author Bashir A.
     * @param  RegisterDevice  $event
     * @return void
     */
    public function handle(RegisterDevice $event)
    {
        if ($event->request->filled('unique_device_id')) {
            $device = Device::find($event->request->unique_device_id);
            if(!$device){
                $device = new Device;
                $device->user_id = $event->user->user_id;
            }                    
            $device->device_id = $event->request->input('unique_device_id', $device->device_id);
            $device->so_version = $event->request->input('so_version', $device->so_version);
            $device->platform = $event->request->input('so_platform', $device->platform);
            $device->device_model_name = $event->request->input('device_model_name', $device->device_model_name);
            $device->device_model_code = $event->request->input('device_model_code', $device->device_model_code);
            $device->device_status_id = 1;

            if ($event->request->filled('latitude') && $event->request->filled('longitude')) {
               $device->latitude = $event->request->latitude;
               $device->longitude = $event->request->longitude;
            }

            if ($event->request->filled('device_token')) {
                $device->notification_token = $event->request->device_token;
            }

            $device->save();
        }        
    }
}
