<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryPicture extends Model
{
    
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'picture_id';

    protected $fillable = ['privacy_status', 'total_likes', 'total_views', 'description', 'image_url', 'source', 'position'];

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "gallery_picture";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | SCOPES
    |---------------------------------------------------------------------------------------
    */

    /**
     * This scope selects gets the pictures of a user by a given size.
     * 
     * @author Bashir A.
     *
     **/
    public function scopeBySize($query, $size){

        return $query->where("image_url", "like", "%_{$size}.%")->orderBy('position', 'asc');

    }


    /*
    |---------------------------------------------------------------------------------------
    | ACCESSORS
    |---------------------------------------------------------------------------------------
    */

    /**
     * This accessor gets the full url to get an image.
     * 
     * @author Bashir A.
     *
     **/
    public function getImageUrlAttribute($value)
    {        
        $name = str_replace('<root_url>/', '', $value);
        return url('api/resources', $name);
    }
}
