<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HealthData extends Model
{
    
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'health_data_id';

    /**
     * Define wich fields are protected for mass assignment.
     *
     * @var string
     **/
    protected $guarded = [];

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "health_data";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | SCOPES
    |---------------------------------------------------------------------------------------
    */

    /**
     * This scope selects Keys, Values, User_id and Created_at date from health_data table.
     * The Values are converted according to the $distanceIn variable.
     * 
     * @author Bashir A.
     *
     **/
    public function scopeHealthDataValuesIn($query, $distanceIn){
        
        $value_distance = "value";

        if ($distanceIn == "km") {
            $value_distance = "ROUND(value/0.62137)";
        }

        $sqlSelectQuery = "IF(health_key = 'avg_distance_walking',{$value_distance},value) AS value";
        
        return $query->select("user_id","health_key",DB::raw($sqlSelectQuery),"created_at");
    }
}
