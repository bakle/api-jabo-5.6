<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportDeleteAccount extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'delete_id';
    protected $guarded = [];


    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "report_delete_account";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }

   
}
