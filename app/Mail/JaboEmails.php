<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JaboEmails extends Mailable
{
    use Queueable, SerializesModels;

    private $email_type;
    private $data;
    private $extra_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_type, $data, $extra_data)
    {
        $this->email_type = $email_type;
        $this->data = $data;
        $this->extra_data = $extra_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*
        |---------------------------------------------------------------------------------------
        | Send Email According The Email Type 
        |---------------------------------------------------------------------------------------
        */

        switch ($this->email_type) {
            case 'feedback':
                return $this->to(env('FEEDBACK_EMAIL'))
                ->from(env('NOREPLY_EMAIL'), 'Jabo')
                ->subject('Jabo Feedback')
                ->markdown('mails.feedback_email', config('mail.markdown'))
                ->withData($this->data)
                ->withUser($this->extra_data);
            break;
        }
    }
}
