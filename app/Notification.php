<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'notification_id';

    protected $guarded = [];

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "notification";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | RELATIONSHIPS 
    |---------------------------------------------------------------------------------------
    */

    public function notificationType()
    {
        return $this->hasOne(NotificationType::class, "notification_type_id", "notification_type");
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'item_id');
    }

    /*
    |---------------------------------------------------------------------------------------
    | Scopes 
    |---------------------------------------------------------------------------------------
    */

    public function scopePending($query)
    {
        return $query->where("viewed_status",0)->where("enabled",1)->where('notification_type', '!=', 12)->where('notification_type', '!=', 16);
    }


    /*
    |---------------------------------------------------------------------------------------
    | Methods
    |---------------------------------------------------------------------------------------
    */

    /**
     * Insert username into notification message.     
     *
     * @param string $username
     * @return string
     * 
     **/    
    public function transformNotificationMessage($username)
    {
        return str_replace("<username>",$username, $this->text);
    }
}
