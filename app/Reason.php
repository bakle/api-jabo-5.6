<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    
    protected $primaryKey = 'reason_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "reason";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
