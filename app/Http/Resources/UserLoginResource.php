<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserLoginResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($user)
    {
        return parent::toArray([
            'user_id' => $user->user_id,
            'weight' => $user->weight,
            'weight_value' => $user->weight_value,
            'height' => $user->height,
            'height_value' => $user->height_value,
            'gender' => $user->gender,
            'facebook_id' => $user->facebook_id,
            'birthday' => $user->birthday,
            'about_me' => $user->about_me,
            'total_views' => $user->total_views,
            'latitude' => $user->latitude,
            'longitude' => $user->longitude,
            'last_name' => $user->last_name,
            'first_name' => $user->first_name,
            'email' => $user->email,
            'total_facebook_friends' => $user->total_facebook_friends,
            'last_activity' => $user->last_activity,
            'idle' => $user->idle,
            'matchpass_count' => $user->matchpass_count,
            'created_at' => (string) $user->created_at,
            'pictures' => transform_pictures($user->pictures),

        ]);
    }
}
