<?php

namespace App\Http\Controllers;

use App\User;
use App\Location;
use Illuminate\Http\Request;
use App\Events\RegisterDevice;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller
{
    
    /**
     * Validate if a user is in the waitlist
     * according the user's location.
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->lat
     * @param string $request->long
     * @param string $request->unique_device_id (and other device data) (optional)
     * @return Json     
     **/
    public function ValidateUserWaitlistByLocation(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'lat' => 'required|numeric',
            'long' => 'required|numeric'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Retreive The User By ID Or Fail IF Doesn't Exists 
        |---------------------------------------------------------------------------------------
        */

        $user = User::with([
            'settings' => function($query) {
                $query->wherePivot("setting_id", 17);
            }            
        ])->findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Register/Update Device And Session  If unique_device_id Exists
        |---------------------------------------------------------------------------------------
        */

        if ($request->has('unique_device_id')){
            event(new RegisterDevice($request, $user));
        }

        /*
        |---------------------------------------------------------------------------------------
        | Check If User Can Skip The Validation Waitlist System. If Is True, Then Return 
        | Succesfully
        |---------------------------------------------------------------------------------------
        */

        $canSkipValidation = $user->settings->first()->pivot->value;

        if ($canSkipValidation) {
           return respondSuccessfully();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get Location By Coordinates.
        |---------------------------------------------------------------------------------------
        */

        $location = app('geocoder')->reverse($request->lat, $request->long)->get()->first();

        /*
        |---------------------------------------------------------------------------------------
        | Get Country 
        |---------------------------------------------------------------------------------------
        */

        $country = $location->getCountry()->getName();
        
        /*
        |---------------------------------------------------------------------------------------
        | Get State 
        |---------------------------------------------------------------------------------------
        */
        
        $state = optional($location->getAdminLevels()->first())->getName();

        /*
        |---------------------------------------------------------------------------------------
        | If There is an Admin Level 2, Then Retrieve The Zone Name, If Not, Assign The Locality
        | Value To The Zone Variable
        |---------------------------------------------------------------------------------------
        */
        if (count($location->getAdminLevels()) > 1) {
            $zone = $location->getAdminLevels()->get(2)->getName();
        }
        else {
            $zone = $location->getLocality();
        }

        /*
        |---------------------------------------------------------------------------------------
        | If Country Is In The Available Countries, Then The Status Is 1 (This Validate If The 
        | App Is Available To Use In These Countries)
        |---------------------------------------------------------------------------------------
        */

        $status = 0;

        if (in_array($country, config('availablecountries'))) {
            $status = 1;
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get The Record Of The Location In The Database That Match The Country, State And Zone.
        |---------------------------------------------------------------------------------------
        */

        $existingLocation = Location::where('country', $country)->where('state', $state)->where('zone', $zone)->first();

        /*
        |---------------------------------------------------------------------------------------
        | If The Location Doesn't Exists, Then Create It 
        |---------------------------------------------------------------------------------------
        */

        if (!$existingLocation) {
            $existingLocation = Location::create([
                'country' => $country,
                'state' => $state,
                'zone' => $zone,
                'status' => $status,
            ]);
        }
        else {
            $existingLocation->status = $status;
            $existingLocation->save();
        }

        /*
        |---------------------------------------------------------------------------------------
        | ASsign Location To The User
        |---------------------------------------------------------------------------------------
        */

        $user->location()->sync([$existingLocation->id]);

        /*
        |---------------------------------------------------------------------------------------
        | If Status Is 1 (User Is Available To Use The App) , Then Return Sucessfully, If Not,
        | Return Result Not Found
        |---------------------------------------------------------------------------------------
        */
        
        if ($existingLocation->status == 1) {
            return respondSuccessfully();
        }

        return respondResultsNotFound();
        

    }
}
