<?php

namespace App\Http\Controllers;

use App\User;
use App\Match;
use App\DataPush;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    /**
     * Count Total Pending Notifications.
     * 
     * This service count the total matches of 
     * a user with the pending notifications
     *
     * @author Bashir A.
     * 
     * @param integer $request->user_id
     * @return json     
     **/
    public function countPendingNotifications(Request $request)
    {        
        
        /*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',    		
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    	    return respondIncorrectParameters();
    	}


    	/*
    	|---------------------------------------------------------------------------------------
    	| Get Total Matches Of The User With The Pending Notifications
    	|---------------------------------------------------------------------------------------
        */
        
        $total_matches_with_pending_notifications = Match::where('user_id', $request->user_id)->whereHas('notifications', function($query) use ($request) {
                $query->where("viewer_user_id", $request->user_id)->pending();
        })->count();
        

        /*
        |---------------------------------------------------------------------------------------
        | Return Data With Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully([
            'total' => $total_matches_with_pending_notifications
        ]);
    	
    }


    /**
     * Mark a notification as viewed By Notification Id.
     * 
     * @author Bashir A.
     * 
     * @param integer $request->notification_id
     * @param integer $request->user_id
     * @return json     
     **/
    public function markNotificationAsViewed(Request $request)
    {        
        
        /*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',
    		'notification_id' => 'required|min:1|numeric',
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    	    return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get Notification By Notification_Id And User_Id, If It Doesn't Exists, Then Throw Fail
        | Error. 
        |---------------------------------------------------------------------------------------
        */

        $notification = Notification::where('viewer_user_id', $request->user_id)->findOrFail($request->notification_id);

        /*
        |---------------------------------------------------------------------------------------
        | Change Viewed Status To True (1) And Save It.
        |---------------------------------------------------------------------------------------
        */

        $notification->viewed_status = 1;
        $notification->save();

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
        
    }


    /**
     * Mark a notification as viewed By Sender Id.
     * And User ID
     * 
     * @author Bashir A.
     * 
     * @param integer $request->user_id
     * @param integer $request->sender_id
     * @return json     
     **/
    public function MarkNotificationAsViewedBySenderId(Request $request)
    {        
        
        /*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',
    		'sender_id' => 'required|min:1|numeric',
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    		return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Update Notifications As Viewed By Sender Id And User Id
        |---------------------------------------------------------------------------------------
        */

        $notification = Notification::where('viewer_user_id', $request->user_id)->where('item_id', $request->sender_id)->update([
            'viewed_status' => 1
        ]);


        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();

    }


    /**
     * Get Last Pending Notifications Grouped By
     * Notification Type, Sender And Viewer
     * 
     * @author Bashir A.
     * 
     * @param integer $request->user_id
     * @param integer $request->index
     * @param integer $request->total
     * @return json     
     **/
    public function getPendingNotification(Request $request)
    {        
        
        /*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',
    		'total' => 'required_with:index',
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    		return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Assign Params To Variables 
        |---------------------------------------------------------------------------------------
        */

        $index = $request->index;
        $total = $request->total;

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id With Notifications, If The USer Doesn't Exists, Then Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $notification_query = Notification::with([
            'notificationType',
            'sender' => function($query) {
                $query->select('user_id', 'last_name', 'first_name', 'gender');
            },
            'sender.pictures' => function($query) {
                $query->bySize(100)->select('owner_user_id', 'image_url');
            }
        ])->pending()->where('viewer_user_id', $request->user_id)->oldest('notification_id');

        /*
		|---------------------------------------------------------------------------------------
        | Get The "Total" Amount Of Notifications If "Index" Doesn't Exists. 
        | If Both ("Index" And "Total") Exists, Then Skip "Index" And Take "Total"
		|---------------------------------------------------------------------------------------
		*/
			
		if(!$index && $total){
		  	$notification_query = $notification_query->take($total);
        }
		elseif($index && $total){
			$notification_query = $notification_query->skip($index)->take($total);                    
		}
        
        /*
        |---------------------------------------------------------------------------------------
        | Get Notifications
        |---------------------------------------------------------------------------------------
        */
            
        $notification = $notification_query->get();

        /*
        |---------------------------------------------------------------------------------------
        | Transform Notification To Set The Required Output Fields 
        |---------------------------------------------------------------------------------------
        */

        $notification->transform(function($item, $key) {
            $data['notification_id'] = (string) $item->notification_id;
            $data['notification_type'] = (string) $item->notification_type;
            $data['tittle'] = (string) $item->tittle;
            $data['text'] = (string) $item->transformNotificationMessage(optional($item->sender)->first_name);
            $data['message'] = (string) $item->transformNotificationMessage(optional($item->sender)->first_name);
            $data['viewed_status'] = (string) $item->viewed_status;
            $data['user_id'] = (string) optional($item->sender)->user_id;
            $data['enabled'] = (string) $item->enabled;
            $data['created_at'] = (string) $item->created_at;
            $data['updated_at'] = (string) $item->updated_at;
            $data['notification_type_id'] = (string) $item->notification_type;
            $data['name'] = (string) $item->notificationType->name;
            $data['description'] = (string) $item->notificationType->description;
            $data['status'] = (string) $item->notificationType->status;
            $data['first_name'] = (string) optional($item->sender)->first_name;
            $data['last_name'] = (string) optional($item->sender)->last_name;
            $data['image_url'] = (string) count(optional($item->sender)->pictures) > 0 ? $item->sender->pictures->first()->image_url : "";
            $data['gender'] = (string) optional($item->sender)->gender;
            $data['destination'] = (string) $item->notificationType->notification_destination_id;
            
            return $data;
        });

        /*
        |---------------------------------------------------------------------------------------
        | If There Are Notifications, Return Them, If Not, Return Not Found
        |---------------------------------------------------------------------------------------
        */
            
        if(count($notification) > 0){
            $data = [
                'count' => $notification->count(),
                'items' => $notification
            ];
            
            return respondSuccessfully($data);
        }
            
        return respondResultsNotFound();
    }


    /**
     * Store the notifications received by the app
     * 
     * @author Bashir A.
     * 
     * @param mixed $request->data 
     * @return json     
     **/
    public function storeNotificacionReceived(Request $request)
    {        
        
        /*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'data' => 'required|',    		
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    		return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Store The Data 
        |---------------------------------------------------------------------------------------
        */

        DataPush::create([
            'data' => $request->data
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }
}
