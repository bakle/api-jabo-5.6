<?php

namespace App\Http\Controllers;

use App\User;
use App\Reason;
use Carbon\Carbon;
use App\AccountDeleted;
use Illuminate\Http\Request;
use App\Events\RegisterDevice;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\SettingController;

class AccountController extends Controller
{
    
    /**
     * Login a user by facebook
     *
     * @author Bashir A.
     * 
     * @param  string  $request->facebook_id
     * @param  string  $request->first_name
     * @param  string  $request->last_name
     * @param  string  $request->email
     * @param  string  $request->gender
     * @param  string  $request->middle_name
     * @param  string  $request->city
     * @param  string  $request->state
     * @param  string  $request->country
     * @param  string  $request->about_me
     * @param  string  $request->birthday
     * @param  numeric  $request->latitude
     * @param  numeric  $request->longitude
     * @param  integer  $request->total_facebook_friends.
     * @return Json
     **/
    public function login(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validate = Validator::make($request->all(), [
            'facebook_id' => 'required',
            'last_name' => 'required_without:first_name'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validate->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Assign Last Name To First Name And Make Last Name Empty If First Name Is Empty
        |---------------------------------------------------------------------------------------
        */

        if (!$request->filled('first_name')) {
            $request->first_name = $request->last_name;
            $request->last_name = "";
        }
        
        /*
        |---------------------------------------------------------------------------------------
        | Get The User By Facebook Id With Pictures (size 500px) 
        |---------------------------------------------------------------------------------------
        */

        $user = User::with([
            "pictures" => function ($query) {
                $query->bySize('500');
            }
        ])->where("facebook_id", $request->facebook_id)->first();

        /*
        |---------------------------------------------------------------------------------------
        | Instantiate A New User If Not Exists
        |---------------------------------------------------------------------------------------
        */

        if (!$user) {
            $user = new User;
            
        }

        /*
        |---------------------------------------------------------------------------------------
        | Set User Fields, If The Input Info Exist, Assign That Value, If Not, Assign Values That
        | Are In The Database
        |---------------------------------------------------------------------------------------
        */

        $user->facebook_id = $request->input('facebook_id', $user->facebook_id);
        $user->first_name = $request->input('first_name', $user->first_name);
        $user->last_name = $request->input('last_name', $user->last_name);
        $user->middle_name = $request->input('middle_name', $user->middle_name);
        $user->email = $request->input('email', $user->email);
        $birthday = $request->input('birthday', $user->birthday);
        if ($birthday) {
            $birthday = Carbon::parse($birthday);
        }
        $user->birthday = $birthday;
        $user->city = $request->input('city', $user->city);
        $user->state = $request->input('state', $user->state);
        $user->country = $request->input('country', $user->country);
        $user->about_me = $request->input('about_me', $user->about_me);
        $user->idle = 0;        
        $user->last_activity = Carbon::now();
        $user->total_facebook_friends = $request->input('total_facebook_friends', $user->total_facebook_friends);
        $user->logged = 1;
        $user->latitude = $request->input('latitude', $user->latitude);
        $user->longitude = $request->input('longitude', $user->longitude);
        $user->last_login = Carbon::now();
        $user->total_views = $user->total_views ?? 0;
        $user->total_likes = $user->total_likes ?? 0;
        $user->gender = 0;
        $user->gender_search = 3;
        $setSettings = false;

        if ($request->filled('verified')) {
            $user->verified = $request->verified == "true" ? 1 : 0;
        }
        else {
            $user->verified = $user->verified ?? 0;
        }        

        /*
        |---------------------------------------------------------------------------------------
        | If User Has Deleted The Account Assign New Values
        |---------------------------------------------------------------------------------------
        */        

        if (!$user->status || $user->status == -2) {            
            $setSettings = true;
            $user->weight = 0;
            $user->weight_value = 0;
            $user->height = 0;
            $user->height_value = 0;
            if ($request->filled('gender')) {
                $user->gender = $request->gender == 'male' ? 1: 2;
                $user->gender_search = $request->gender == "male" ? 2 : 1;
            }

        }        
        
        /*
        |---------------------------------------------------------------------------------------
        | Save User And Set Settings If Is Needed
        |---------------------------------------------------------------------------------------
        */
        
        $user->status = 1;
        
        $user->save();
        
        if ($setSettings) {            
            SettingController::setUserSettings($user);
        }
        
        /*
        |---------------------------------------------------------------------------------------
        | Register/Update Device And Session If unique_device_id Exists
        |---------------------------------------------------------------------------------------
        */

        if ($request->has('unique_device_id')){
            event(new RegisterDevice($request, $user));
        }

        /*
        |---------------------------------------------------------------------------------------
        | Transform Output
        |---------------------------------------------------------------------------------------
        */

        $data['user_id'] = (string) $user->user_id;
        $data['weight'] = (string) $user->weight;
        $data['weight_value'] = (string) $user->weight_value;
        $data['height'] = (string) $user->height;
        $data['height_value'] = (string) $user->height_value;
        $data['gender'] = (string) $user->gender;
        $data['facebook_id'] = (string) $user->facebook_id;
        $data['birthday'] = (string) $user->birthday->toDateString();
        $data['about_me'] = (string) $user->about_me;
        $data['total_views'] = (string) $user->total_views;
        $data['latitude'] = (string) $user->latitude;
        $data['longitude'] = (string) $user->longitude;
        $data['last_name'] = (string) $user->last_name;
        $data['first_name'] = (string) $user->first_name;
        $data['email'] = (string) $user->email;
        $data['total_facebook_friends'] = (string) $user->total_facebook_friends;
        $data['last_activity'] = (string) $user->last_activity;
        $data['idle'] = (string) $user->idle;
        $data['matchpass_count'] = (string) $user->matchpass_count;
        $data['created_at'] = (string) $user->created_at;
        $data['pictures'] = transform_pictures($user->pictures);

        /*
        |---------------------------------------------------------------------------------------
        | Return Data With Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully($data);
    }


    /**
     * Delete a user account
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param integer $request->reason_id
     * @param string $request->comments
     * @return Json     
     **/
    public function delete(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::with([
            'settings' => function($query) {
                $query->where(env('DB_API_PREFIX') . 'setting_user.setting_id', 2);
            }
        ])->findOrFail($request->user_id);
        
        /*
        |---------------------------------------------------------------------------------------
        | Get Reason (Reason Of Why The User Is Deleting The Account) By Id 
        |---------------------------------------------------------------------------------------
        */
        
        $reason = null;
        if ($request->filled('reason_id')) {
            $reason = Reason::findOrFail($request->reason_id);
        }

        
        /*
        |---------------------------------------------------------------------------------------
        | Begin Transaction, If Something Goes Wrong Then It Will Rollback All Database Changes 
        |---------------------------------------------------------------------------------------
        */

        DB::transaction(function () use($reason, $user, $request) {

            /*
            |---------------------------------------------------------------------------------------
            | Insert Data Into Delete Account Table 
            |---------------------------------------------------------------------------------------
            */

            $deletedUser = new AccountDeleted;
            $deletedUser->user_id = $user->user_id;
            $deletedUser->reason_id = $reason ? $reason->reason_id : null;
            $deletedUser->comment = $request->comments;
            $deletedUser->save();

            /*
            |---------------------------------------------------------------------------------------
            | Delete Records Of The User From Tables: _swipe, _match_users, _match_pass_sender
            | _setting_user, _activity, _activity_type, _chat_message, _notification
            |---------------------------------------------------------------------------------------
            */
            
            $user->swipesToMe()->orWhere('viewer_id', $user->user_id)->delete();        
            $user->matches()->orWhere('match_user_id', $user->user_id)->delete();
            $user->matchpassToMe()->orWhere('match_user_id', $user->user_id)->delete();        
            $user->settings()->delete();
            $user->activityTypes()->detach();
            $user->activitiesFromMe()->orWhere('match_user_id', $user->user_id)->delete();
            $user->chatMessagesFromMe()->orWhere('match_id', $user->user_id)->delete();
            $user->pictures()->delete();
            $user->healthData()->delete();
            $user->location()->detach();
            $user->notifications()->delete();            
            $user->customActivities()->detach();
            $user->profilesViewed()->orWhere('user_id', $user->user_id)->delete();
            $user->interests()->delete();
            $user->friends()->delete();
            $user->friends_2()->delete();

            /*
            |---------------------------------------------------------------------------------------
            | Set User Status To Deleted 
            |---------------------------------------------------------------------------------------
            */

            $user->status = -2;
            $user->save();

            /*
            |---------------------------------------------------------------------------------------
            | Delete User's Active Sessions
            |---------------------------------------------------------------------------------------
            */
            
            if (count($user->activeSessions) > 0) {
                foreach ($user->activeSessions as $session) {
                    $session->pivot->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }
            }

            /*
            |---------------------------------------------------------------------------------------
            | Get User Location By Coordinates 
            |---------------------------------------------------------------------------------------
            */

            try {
                $user_location = app('geocoder')->reverse($user->latitude, $user->longitude)->get()->first();
                $city = $user_location->getLocality();
            }
            catch(\Exception $e) {
                $city = '';
            }

            /*
            |---------------------------------------------------------------------------------------
            | Insert Records Into _report_delete_account Table
            |---------------------------------------------------------------------------------------
            */

            $user->reportDeleteAccount()->create([
                'full_name' => $user->first_name . " " . $user->last_name,
                'email' => $user->email,
                'facebook_id' => $user->facebook_id,
                'birthday' => $user->birthday,
                'gender' => $user->gender,
                'gender_search' => $user->gender_search,
                'registration_date' => $user->created_at,
                'latitude' => $user->latitude,
                'longitude' => $user->longitude,
                'city' => $city != '' ? $city : $user->city,
            ]);

            /*
            |---------------------------------------------------------------------------------------
            | Delete Pictures Profile Folder 
            |---------------------------------------------------------------------------------------
            */

            ImageController::deletePicturesProfile($user);

        });

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }


    /**
     * Logout user.
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param integer $request->reason_id
     * @param string $request->comments
     * @return Json     
     **/
    public function logout(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Delete User's Active Sessions Of The Current Device
        |---------------------------------------------------------------------------------------
        */
        
        $device_active_sessions = $user->activeSessions()->where('sessions.device_id', $request->unique_device_id)->get();
        if (count($device_active_sessions) > 0) {
            foreach ($user->activeSessions as $session) {
                $session->pivot->update([
                    'deleted_at' => Carbon::now()
                ]);
            }
        }

        /*
        |---------------------------------------------------------------------------------------
        | Log Out User (0 Is Logged Out)
        |---------------------------------------------------------------------------------------
        */

        $user->logged = 0;
        $user->save();

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();

    }
}
