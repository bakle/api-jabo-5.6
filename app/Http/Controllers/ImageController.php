<?php

namespace App\Http\Controllers;

use App\User;
use App\ActivityType;
use Illuminate\Http\Request;
use App\Classes\ImageProcess;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    
    /**
     * Get the image from the server
     *
     * @author Bashir A.
	 * 
     * @param  string  $image
	 * @return Image
     **/
    public function getImage($image)
    {

    	/*
    	|---------------------------------------------------------------------------------------
    	| Set $userId To Empty String,  Get Default Base Image Path, And Get The Image Blur Name
    	|---------------------------------------------------------------------------------------
		*/
		
    	$userId = '';		
		$ext = explode(".", $image);
		$imageNameBlur = substr($image, 0, stripos($image, '.')) . "_blur" . "." . $ext[1];
		$path = config('paths.image_storage');

    	/*
    	|---------------------------------------------------------------------------------------
    	| Get Month Name From Image And Image Size.
    	| Image Size Is Retreived It By Calculating The Position Of The '.' Character And Taking 
    	| 3 Characteres Before The '.' Character. So If The Image Have '7a938_500.jpg',  We 
    	| Count 3 Characters Before '.' Character And Get '500'
    	|---------------------------------------------------------------------------------------
		*/
		
		$imageMonth = null;
		$monthNumber = substr($image,0,2);
		if ($monthNumber >= 1 && $monthNumber <= 12) {
			$imageMonth = strtolower(date("M", mktime(0,0,0,$monthNumber)));
		}

		/*
		|---------------------------------------------------------------------------------------
		| Get Size Of Image Name (_500, _200, etc) 
		|---------------------------------------------------------------------------------------
		*/
    	
    	$imageSize = substr($image, stripos($image, '.') - 3, 3);

    	if ($imageMonth != null) {

    		/*
    		|---------------------------------------------------------------------------------------
    		| Get The Year From The Image
    		|---------------------------------------------------------------------------------------
    		*/

    		$imageYear = substr($image,7,2);

    		if (is_numeric($imageYear)) {

    			/*
    			|---------------------------------------------------------------------------------------
    			| Get The User Id From The Image And Get The Folder Type.
    			| First We Remove The First 12 Characters (After The 12 Character Begin The User Id And
    			| The Folder Type).
    			| Because We Can Know The Lenght Of The User Id, We Loop Through The String Until We Get
    			| A Not Numeric Character (i.e '574p' => '574' Is The User Id And 'p' Is The Folder Type)
    			|---------------------------------------------------------------------------------------
    			*/

    			$imageSubstringArray = str_split(substr($image, 12));

    			foreach ($imageSubstringArray as $item) {
					if (is_numeric($item)) {
						$userId .= $item;
					}
					else {
						$folderType = $this->getTypeFolder($item);						
						break;
					}
				}
				
				/*
				|---------------------------------------------------------------------------------------
				| Set The Path Of Image When Has The Folder Cloud Structure
				|---------------------------------------------------------------------------------------
				*/

				$path = env('PATH_IMAGE_CLOUD').$imageMonth."-".$imageYear."/user-".$userId."/".$folderType."/";
				
    		}
		}
		
		/*
		|---------------------------------------------------------------------------------------
		| If The Blur Image Doesn't Exists, Then Set The Path With The Non Blur Image 
		|---------------------------------------------------------------------------------------
		*/

		if (!File::exists($path.$imageNameBlur)) {
			$path = $path . $image;
		}
		else {
			$path = $path . $imageNameBlur;
		}

    	return $this->searchImage($path,$userId,$imageSize);
    	
    }

    

     /**
     * Get the folder type
     *
     * @author Miguel C.
	 * 
     * @param  string  $type
	 * @return string
     **/
    public function getTypeFolder($type)
    {
		switch($type){
			case 'p':
				$folderType = 'profile-pictures';
			break;
			case 'c':
				$folderType = 'chat-photos';
			break;
			default:
				$folderType = 'no';
			break;
		}
		return $folderType;
	}

	/**
     * Get the image from the given path
     *
     * @author Miguel C.
	 * 
     * @param  string  $name
     * @param  integer  $userId
     * @param  string  @size
     **/
	public function searchImage($name, $userId,$size)
	{
		/*
		|---------------------------------------------------------------------------------------
		| Get The User By Id And Get The Gender Of The User (In Case We Have To Retreive The
		| Default Picture)
		|---------------------------------------------------------------------------------------
		*/

		$user = User::find($userId);		

		if ($user) {
			$gender = $user->gender;
		}
		else {
			$gender = 0;
		}

		/*
		|---------------------------------------------------------------------------------------
		| Validate If Image Exist In The Server And Retrieve It, If Not, Then Retrieve The
		| Default Picture
		|---------------------------------------------------------------------------------------
		*/

		if (File::exists($name)) {

			return response()->file($name);
		}
		
		return $this->getDefaultPicture($gender,$size);
		
	}

	/**
     * Get the default picture when the image doesn't
     * exists in the server
     *
     * @author Miguel C.
	 * 
     * @param  integer  $gender     
     * @param  string  @size
     **/
	public function getDefaultPicture($gender,$size)
	{
		
		switch ($size) {
			case 100:
				if ($gender == 1) {
					$default_image = "default_male_100.jpg";
				}
				else if ($gender == 2) {
					$default_image = "default_female_100.jpg";
				}
				else {
					$default_image = "image_default_100.jpg";
				}
			break;
			case 500:
				if ($gender == 1) {
					$default_image = "default_male_500.jpg";
				}
				else if ($gender == 2) {
					$default_image = "default_female_500.jpg";
				}
				else {
					$default_image = "image_default_500.jpg";
				}
			break;
			default:
				if ($gender == 1) {
					$default_image = "default_male.jpg";
				}
				else if ($gender == 2) {
					$default_image = "default_female.jpg";
				}
				else {
					$default_image = "image_default.jpg";
				}
			break;
		}

		
		$name = config('paths.image_default').$default_image;
		return response()->file($name);
	}


	/**
     * Get the activities icons     
     *
     * @author Bashir A.
	 * 
	 * @param string $image     
     * @param  string  @size
	 * @param  string  $status
	 * @return Image
     **/
	public function getActivitiesIcons($image, $size = '1', $status = 'off')
	{
		
		/*
		|---------------------------------------------------------------------------------------
		| If $image Is Sprite, Then Get The Sprite Name Image 
		|---------------------------------------------------------------------------------------
		*/
		if ($image == 'sprite') {
			$name = config('paths.image_icons') . "Icon-Activities.png";
		}
		else {
			if ($status == '1') {
				$status = 'on';
			}

			$name = "";

			/*
			|---------------------------------------------------------------------------------------
			| Get The Activity Type By Name Or Id
			|---------------------------------------------------------------------------------------
			*/

			$activityType = ActivityType::where('icon_name',$image)->orWhere('activity_type_id',$image)->first();

			if (!empty($activityType)) {
				$image = $activityType->icon_name.".png";
				$name = config('paths.image_icons').$size."/".$status."/".$image;			
			}

			if (!File::exists($name)) {
				$name = config('paths.image_icons').$size."/".$status."/ac-other-icon.png";
				if (!File::exists($name)) {
					$name = config('paths.image_icons')."2/off/ac-other-icon.png";
				}			
			}
			
		}

		return response()->file($name);
	}


	/**
	 * Delete Pictures Profile of a user
	 *
	 * @author Bashir A.
	 * 
	 * @param User $user
	 * @return boolean	 
	 **/
	public function deletePicturesProfile(User $user, $filename = null)
	{
		$monthName = strtolower($user->created_at->shortEnglishMonth);
		$path = config('paths.image_cloud') . $monthName . '-' . $user->created_at->format('y') . '/user-' . $user->user_id;

		/*
		|---------------------------------------------------------------------------------------
		| If File Name Exists, Then Delete Only That File 
		|---------------------------------------------------------------------------------------
		*/

		if ($filename) {
			$path .= '/profile-pictures/' . $filename;
		}

		/*
		|---------------------------------------------------------------------------------------
		| If File Exists, Then Delete It. If Filename Is Set Then Delete File, If Not, Delete 
		| Directory
		|---------------------------------------------------------------------------------------
		*/

		if (File::exists($path)) {
			if ($filename) {
				File::delete($path);
			}
			else {
				File::deleteDirectory($path);
			}			
		}

		return true;
	}


	/**
	 * Upload Pictures Profile of a user
	 *
	 * @author Bashir A.
	 * 
	 * @param User $user
	 * @param Image $file
	 * @return string	 
	 **/
	public function uploadProfilePicture(User $user, UploadedFile $file = null)
	{
		
		/*
		|---------------------------------------------------------------------------------------
		| Get Month Name (Only 3 First Letters Like "'Feb' Or 'Mar' Or 'Jun'" )
		| Of User Registration Date And Short Year (18 instead of 2018)
		|---------------------------------------------------------------------------------------
		*/

		$monthName = strtolower($user->created_at->shortEnglishMonth);
		$shortYear = $user->created_at->format('y');

		/*
		|---------------------------------------------------------------------------------------
		| Set The File Name According Different Parameters Like Month, Year, Random Strings
		| And Hashes.
		|---------------------------------------------------------------------------------------
		*/

		$filename = $user->created_at->format('m') . str_random(5) . $shortYear . str_random(3) . $user->user_id . "p" . hash("md5", now());

		/*
		|---------------------------------------------------------------------------------------
		| Get The Image Type (Extension) 
		|---------------------------------------------------------------------------------------
		*/

		$imageExtension = $file->getClientOriginalExtension();
		
		/*
		|---------------------------------------------------------------------------------------
		| Set Destination Path To Save The Image 
		|---------------------------------------------------------------------------------------
		*/

		$destinationPath = "/mnt/images/" . $monthName . "-" . $shortYear . "/user-" . $user->user_id . "/profile-pictures";

		/*
		|---------------------------------------------------------------------------------------
		| Upload Original Image 
		|---------------------------------------------------------------------------------------
		*/

		/*
		|---------------------------------------------------------------------------------------
		| Upload The Original Image File
		|---------------------------------------------------------------------------------------
		*/
		
		$file->move($destinationPath, $filename . "." . $imageExtension);

		/*
		|---------------------------------------------------------------------------------------
		| Transform And Save Image To 500px
		|---------------------------------------------------------------------------------------
		*/

		ImageProcess::transformAndSave($destinationPath, $filename, $imageExtension, 500);

		/*
		|---------------------------------------------------------------------------------------
		| Transform And Save Image To 100px
		|---------------------------------------------------------------------------------------
		*/

		ImageProcess::transformAndSave($destinationPath, $filename, $imageExtension, 100);


		/*
		|---------------------------------------------------------------------------------------
		| Return Generic File Name 
		|---------------------------------------------------------------------------------------
		*/

		return "<root_url>/" . $filename . "<size>" . "." . $imageExtension;
	}


	/**
	 * Get Pictures Profiles Of A User
	 *
	 *
	 * @param integer $request->user_id
	 * @param integer $request->index
	 * @param integer $request->total
	 * @return type
	 * @throws conditon
	 **/
	public function getProfilePictures(Request $request)
	{
		
		/*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',
    		'viewer_id' => 'required|min:1|numeric',
    		'total' => 'required_with:index',
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    		return respondIncorrectParameters();
		}
		

		/*
		|---------------------------------------------------------------------------------------
		| Get User And Viewer, If They Doesn't Existsm Then Throw Fail Error 
		|---------------------------------------------------------------------------------------
		*/

		$user = User::findOrFail($request->user_id);
		$viewer = User::findOrFail($request->viewer_id);

        /*
        |---------------------------------------------------------------------------------------
        | Assign Params To Variables 
        |---------------------------------------------------------------------------------------
        */

        $index = $request->index;
		$total = $request->total;
		
		/*
		|---------------------------------------------------------------------------------------
		| Get Pictures According Index An Total Values
		|---------------------------------------------------------------------------------------
		*/

		$pictures = $user->pictures()->bySize('500')->where('position', '!=', 0)->whereNotNull('position')->orderBy('position', 'asc');

		if (!$index && !$total) {
			$pictures = $pictures->get();
		}

		if ($index && !$total) {
			$pictures = $pictures->skip($index)->get();
		}

		if (!$index && $total) {
			$pictures = $pictures->take($total)->get();
		}
		if ($index && $total) {
			$pictures = $pictures->skip($index)->take($total)->get();
		}


		/*
		|---------------------------------------------------------------------------------------
		| Transform Output Data 
		|---------------------------------------------------------------------------------------
		*/

		$pictures->transform(function($item) {
			$data['picture_id'] = (string) $item->picture_id;
			$data['owner_user_id'] = (string) $item->owner_user_id;
			$data['image_url'] = (string) $item->image_url;
			$data['source'] = (string) $item->source;
			$data['position'] = (string) $item->position;
			$data['created_at'] = (string) $item->created_at;
			return $data;
		});


		/*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully([
			'total' => count($pictures),
			'items' => $pictures
		]);
	}


	
}
