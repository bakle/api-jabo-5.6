<?php

namespace App\Http\Controllers;

use App\User;
use App\ViewProfile;
use Illuminate\Http\Request;
use App\Events\RegisterDevice;
use App\Http\Controllers\Controller;
use Facades\App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    
    /**
     * Get the information(profile) of a user.
     *
     * @author Bashir A.
     * 
     * @param  integer  $request->user_id
     * @param  integer  $request->viewer_id
     * @return Json
     **/
    public function getProfile(Request $request)
    {
    	
    	/*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
    	*/

    	$validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',
    		'viewer_id' => 'required|min:1|numeric'
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    		return respondIncorrectParameters();
    	}


    	/*
    	|---------------------------------------------------------------------------------------
    	| Get Viewer Info With Settings By Id, If Doesn't Exists, Throw Fail Error.
    	|---------------------------------------------------------------------------------------
    	*/

    	$viewer = User::with("settings")->findOrFail($request->viewer_id);

    	/*
        |---------------------------------------------------------------------------------------
        | Get Viewer Settings
        |---------------------------------------------------------------------------------------
        */

        $setting_show_height_in = $viewer->settings->where("name", "show_height_in")->first()->pivot->value;
        $setting_show_weight_in = $viewer->settings->where("name", "show_weight_in")->first()->pivot->value;
        $setting_show_distance_in = $viewer->settings->where("name", "show_distance_in")->first()->pivot->value;
        $setting_search_distance = $viewer->settings->where("name", "search_distance")->first()->pivot->value;

        /*
        |---------------------------------------------------------------------------------------
        | Get User Info With Pictures (500px Size), Health Data And Common Activities With 
        | The Viewer
        |---------------------------------------------------------------------------------------
        */

        $user = User::with([
        			"pictures" => function ($query) {
        				$query->bySize('500');
        			},
                    "activityTypes" => function ($query) use ($viewer) {                        
                        $query->commonActivites($viewer->user_id);                                             
                    },
                    "healthData" => function ($query) use ($setting_show_distance_in) {
                    	$query->healthDataValuesIn($setting_show_distance_in);
                    }
                ])->where("status", 1)->find($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | If User Doesn't Exists, Then Return Results Not Found Status 
        |---------------------------------------------------------------------------------------
        */

        if (!$user) {
 			return respondResultsNotFound();
 		}

        /*
        |---------------------------------------------------------------------------------------
        | If User Has Been Blocked By The Viewer, The Return User Blocked Status 
        |---------------------------------------------------------------------------------------
        */

 		if ($user->isBlockedBy($viewer)) {
        	return respondUserBlocked();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get Common Interests Between User And Viewer
        |---------------------------------------------------------------------------------------
        */

        $common_interests = $user->interests()->commonInterests($viewer->user_id)->select("fitdate_user_interests.name")->get();

        /*
        |---------------------------------------------------------------------------------------
        | Get Common Friends(With Related User And Pictures) Between The User And The Viewer
        |---------------------------------------------------------------------------------------
        */
        
        $common_friends = $user->friends()->commonFriends($viewer->facebook_id)->with([
            "user", 
            "user.pictures" => function ($query) {
                $query->bySize('500');
            }
        ])->get();

        /*
        |---------------------------------------------------------------------------------------
        | Transform Common Friends Data (Just Setting The Data We Need Like Name, Picture And Level)
        |---------------------------------------------------------------------------------------
        */

        $friends_level_1 = transform_friends($common_friends, '1');

        /*
        |---------------------------------------------------------------------------------------
        | Get User Friends (With Their Friends) Without The Common Friends
        | Get Viewer Friends (With Their Friends) Without The Common Friends
        |---------------------------------------------------------------------------------------
        */
        
        $user_friends = $user->friends()->withFriends()->whereNotIn("friend_id", $common_friends->pluck("friend_id"))->get();

        $viewer_friends = $viewer->friends()->withFriends()->whereNotIn("friend_id", $common_friends->pluck("friend_id"))->get();

        /*
        |---------------------------------------------------------------------------------------
        | Check If User And Viewer Are Direct Friend, If Not, Then Set Second Level Friends
        |---------------------------------------------------------------------------------------
        */

        if (!count($user_friends->where("friend_id", $viewer->facebook_id)) && !count($viewer_friends->where("friend_id", $user->facebook_id))) {
            
            if (count($user_friends) > 0) {
                
                $friends_second_level = $user_friends->map(function ($item, $value) use ($viewer_friends) {
                    if (count((optional($item->user)->friends))) {
                        foreach ($item->user->friends as $friend_user_friends) {                            
                            if (count($viewer_friends->where("friend_id", $friend_user_friends->friend_id)) > 0) {
                                $data[] = $viewer_friends->where("friend_id", $friend_user_friends->friend_id)->first();
                            }
                        }
                    }

                    if (isset($data)) {
                        return $data;
                    }                    
                    
                })->filter();                
            }
        }

        /*
        |---------------------------------------------------------------------------------------
        | Transform Second Level Friends Data. (Just Setting The Data We Need Like Name, 
        | Picture And Level)
        |---------------------------------------------------------------------------------------
        */

        if (isset($friends_second_level)) {
            $friends_level_2 = transform_friends($friends_second_level, '2');            
            $common_friends = collect($common_friends)->merge($friends_level_2);
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get Distance Between User And Viewer
        |---------------------------------------------------------------------------------------
        */

        $distance = get_distance($setting_show_distance_in, $viewer->latitude, $viewer->longitude, $user->latitude, $user->longitude);

        /*
        |---------------------------------------------------------------------------------------
        | Transform Output
        |---------------------------------------------------------------------------------------
        */

        $user_info['user_id'] = (string) $user->user_id;
        $user_info['weight'] = format_weight_in($setting_show_weight_in, $user->weight_value);
        $user_info['weight_value'] = (string) $user->weight_value;
        $user_info['height'] = format_height_in($setting_show_height_in, $user->height_value);
        $user_info['height_value'] = (string) $user->height_value;
        $user_info['gender'] = (string) $user->gender;
        $user_info['facebook_id'] = (string) $user->facebook_id;
        $user_info['birthday'] = (string) $user->birthday;
        $user_info['about_me'] = (string) $user->about_me;
        $user_info['total_views'] = (string) $user->total_views;
        $user_info['latitude'] = (string) $user->latitude;
        $user_info['longitude'] = (string) $user->longitude;
        $user_info['distance'] = (string) $distance;
        $user_info['last_name'] = (string) $user->last_name;
        $user_info['first_name'] = (string) $user->first_name;
        $user_info['email'] = (string) $user->email;
        $user_info['gender_search'] = (string) $user->gender_search;
        $user_info['created_at'] = (string) $user->created_at;
        $user_info['total_facebook_friends'] = (string) $user->total_facebook_friends;
        $user_info['last_activity'] = (string) $user->last_activity;
        $user_info['idle'] = (string) $user->idle;
        $user_info['matchpass_count'] = (string) $user->matchpass_count;
        $user_info['health_info'] = transform_health_data($user->healthData, $viewer->settings);
        $user_info['pictures'] = transform_pictures($user->pictures);
        $user_info['common_activities'] = transform_activities($user->activityTypes);
        $user_info['common_friends'] = $common_friends;
        $user_info['common_interests'] = $common_interests;

        /*
        |---------------------------------------------------------------------------------------
        | Return Data With Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully($user_info);
    }
    


    /**
     * Edit the profile of a user
     *
     * @author Bashir A.
     * 
     * @param  integer  $request->user_id
     * @param  string  $request->about_me
     * @param  string  $request->gender
     * @return Json
     **/
    public function editProfile(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | If about_me Input Exists And Is Not Empty, Then Assign The Value To The User
        |---------------------------------------------------------------------------------------
        */

        if ($request->filled('about_me')) {
            $user->about_me = $request->about_me;
        }

        /*
        |---------------------------------------------------------------------------------------
        | If gender Input Exists And Is Not Empty, Then Assign The Value To The User
        |---------------------------------------------------------------------------------------
        */
        
        if ($request->filled('gender')) {
            $user->gender = $request->gender;
        }

        /*
        |---------------------------------------------------------------------------------------
        | Save User's New Values 
        |---------------------------------------------------------------------------------------
        */

        $user->save();

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }
    


    /**
     * Set Idle of a user
     * This service set if the user is online or not.
     * Also provide the last time the user used the app
     * and update the coordinates of the device that the user
     * is using.
     *
     * @author Bashir A.
     * 
     * @param integer $request->user_id
     * @param integer $request->idle
     * @param double $request->latitude
     * @param double $request->longitude
     * @param string $request->unique_device_id
     * @return Json     
     **/
    public function setIdle(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'idle' => 'required|numeric|between:0,1'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | If Inputs Latitude And Longitude Are Not Empty, Then Set User Latitude And Longitude
        | Values.
        |---------------------------------------------------------------------------------------
        */

        if ($request->filled('latitude') && $request->filled('longitude')) {
            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
        }

        /*
        |---------------------------------------------------------------------------------------
        | Dispatch Register Device Event
        |---------------------------------------------------------------------------------------
        */            

        if ($request->filled('unique_device_id')) {
            event(new RegisterDevice($request, $user));
        }

        /*
        |---------------------------------------------------------------------------------------
        | Set last_activity and idle in users table
        |---------------------------------------------------------------------------------------
        */

        $user->last_activity = now();
        $user->idle = $request->idle;

        $user->save();

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();

    }


    /**
     * Add user's device token
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->device_token
     * @param string $request->unique_device_id (and other device data) (optional)
     * @return Json     
     **/
    public function addDeviceToken(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'device_token' => 'required'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Remove blank spaces in device token string And Register Device And Session
        |---------------------------------------------------------------------------------------
        */

        $device_token = str_replace(" ", "", $request->device_token);
        $user->device_token = $device_token;
        $user->save();

        if ($request->has('unique_device_id')){
            event(new RegisterDevice($request, $user));
        }

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }


    /**
     * Mark a profile as viewed.
     * 
     * This service add a record in the database 
     * of wich profile has been viewed by the user.
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->viewer_id
     * @return Json     
     **/
    public function viewProfile(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'viewer_id' => 'required|numeric'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }


        /*
        |---------------------------------------------------------------------------------------
        | Get User And Viewer By Id, If Any Of Them Doesn't Exists, Then Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);
        $viewer = User::findOrFail($request->viewer_id);

        ViewProfile::firstOrCreate([
            'user_id' => $user->user_id,
            'viewer_id' => $viewer->user_id
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }


    /**
     * Upload Image Profile
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param mixed $request->image0
     * @return Json     
     **/
    public function UploadPicture(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'image0' => 'required|image',
            'position' => 'required|min:1|max:6'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        

        /*
        |---------------------------------------------------------------------------------------
        | Get User And Viewer By Id, If Any Of Them Doesn't Exists, Then Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);
        
        /*
        |---------------------------------------------------------------------------------------
        | Upload The Image 
        |---------------------------------------------------------------------------------------
        */

        $filename = ImageController::uploadProfilePicture($user, $request->image0);
        
        /*
        |---------------------------------------------------------------------------------------
        | Get The Source Of The Image, If Not Set, Then Set It To Facebook 
        |---------------------------------------------------------------------------------------
        */

        $source = $request->input('source', 'facebook');

        /*
        |---------------------------------------------------------------------------------------
        | Get The  Position Parameter. Position Must Be Between 1 And 6 (This is The Position
        | Where The Image Should Position In The App)
        |---------------------------------------------------------------------------------------
        */

        $position = $request->position;        

        /*
        |---------------------------------------------------------------------------------------
        | If There Is A Position, Then Delete The Images From Folder And From The Database Of
        | That Position
        |---------------------------------------------------------------------------------------
        */

        if ($position) {
            $user_pictures = $user->pictures()->where('position', $position)->get();
            if (count($user_pictures) > 0) {
                foreach ($user_pictures as $picture) {
                    $filename = str_replace('<root_url>/', '', $picture->getOriginal('image_url'));
                    ImageController::deletePicturesProfile($user, $filename);
                }
                
                $user->pictures()->where('position', $position)->delete();
            }
        }

        /*
        |---------------------------------------------------------------------------------------
        | If There Is No Position ($position), Then Get The Pictures Of First Position, If
        | There Is No Pictures On First Position, Then Set The Position To 1. If There Are
        | Pictures On First Position Then Delete The Pictures On 6th Position To Add The New
        | Picture (If There Is No Picture On 6th Position, The Picture Will Be Stored On The Last
        | Position).
        |---------------------------------------------------------------------------------------
        */

        else {
            $user_pictures = $user->pictures()->where('position', 1)->get();

            if (count($user_pictures) > 0) {
                for ($i = 6; $i > 0; $i--) {
                    $user_pictures_on_position = $user->pictures()->where('position', $i)->get();

                    if (count($user_pictures_on_position) > 0) {

                        /*
                        |---------------------------------------------------------------------------------------
                        | If There Are Pictures On 6th Position Then Delete Them, If Not, Then Update Position
                        | On The Pictures
                        |---------------------------------------------------------------------------------------
                        */

                        if ($i == 6) {
                            foreach ($user_pictures_on_position as $picture) {
                                $filename = str_replace('<root_url>/', '', $picture->getOriginal('image_url'));
                                ImageController::deletePicturesProfile($user, $filename);
                            }

                            $user->pictures()->where('position', $i)->delete();
                            $position = 6;
                        }
                        else {
                            $user->pictures()->where('position', $i)->update([
                                'position' => ($i + 1)
                            ]);
                            $position = $i;
                        }
                    }
                }

                
            }
            else {
                $position = 1;
            }
        }


        /*
        |---------------------------------------------------------------------------------------
        | Insert Pictures Records On Data base (Original, 100px And 500px)
        |---------------------------------------------------------------------------------------
        */
        
        $picture_sizes = ['', '_100', '_500'];
        foreach ($picture_sizes as $size) {
            $user->pictures()->create([
                'privacy_status' => 0,
                'total_likes' => 0,
                'total_views' => 0,
                'description' => "",
                'image_url' => str_replace('<size>', $size, $filename),
                'source' => $source,
                'position' => $position,
            ]);
        }
        

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }


    /**
     * Delete a Image Profile
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param integer $request->position
     * @return Json     
     **/
    public function DeletePicture(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',            
            'position' => 'required|min:1|max:6'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }


        /*
        |---------------------------------------------------------------------------------------
        | Get User By ID, If There Is NO User, Then Throw Fail Error 
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Get Profile Pictures By Position, If They Don't Exist, Then Throw Failed Error 
        |---------------------------------------------------------------------------------------
        */

        $pictures = $user->pictures()->where('position', $request->position)->get();
        if (!count($pictures)) {
            return respondFailedOperation();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Delete Pictures On DB And Folder According The Position 
        |---------------------------------------------------------------------------------------
        */

        $user->pictures()->where('position', $request->position)->delete();
        foreach ($pictures as $picture) {
            $filename = str_replace('<root_url>/', '', $picture->getOriginal('image_url'));
            ImageController::deletePicturesProfile($user, $filename);
        }

        /*
        |---------------------------------------------------------------------------------------
        | Reposition The Pictures If The Deleted Pictures Wasn't In The Last Position.
        | First, We Get All Pictures With Position Greater Than The Deleted Picture.
        | If There Are Pictures, Then We Move All Pictures 1 Position Below The Actual Position.
        |---------------------------------------------------------------------------------------
        */

        $upper_position_pictures = $user->pictures()->where('position', '>', $request->position)->get();
        if (count($upper_position_pictures) > 0) {
            foreach ($upper_position_pictures as $picture) {
                $picture->update([
                    'position' => ($picture->position - 1)
                ]);
            }
        }


        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
    }
}
