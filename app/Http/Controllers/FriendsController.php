<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FriendsController extends Controller
{
    

    /**
     * Set user's friends
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->facebook_ids
     * @return Json     
     **/
    public function setFriends(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',            
            'facebook_ids' => 'required'
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }


        /*
        |---------------------------------------------------------------------------------------
        | Get User By ID, If There Is NO User, Then Throw Fail Error 
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Transform Facebook Ids To Array
        |---------------------------------------------------------------------------------------
        */

        $facebook_ids = str_replace("\\", "", explode("\\n", $request->facebook_ids));        

        /*
        |---------------------------------------------------------------------------------------
        | Clean User's Friends Records From DB 
        |---------------------------------------------------------------------------------------
        */

        $user->friends_2()->delete();

        /*
        |---------------------------------------------------------------------------------------
        | Insert The Records On The Table 
        |---------------------------------------------------------------------------------------
        */

        foreach ($facebook_ids as $facebook_id) {

            $user->friends_2()->create([
                'friend_id' => $facebook_id,
                'status' => 1,
            ]);            
        }        

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();

    }

}
