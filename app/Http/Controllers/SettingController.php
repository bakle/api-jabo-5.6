<?php

namespace App\Http\Controllers;

use App\User;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Set User Settings
     *     
     * @author Bashir A.
     * 
     * @param User $user
     * @return void     
     **/
    public function setUserSettings(User $user)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Get All Users Settings Types 
        |---------------------------------------------------------------------------------------
        */

        $settings = Setting::whereHas('settingGroup',function($query){
            $query->where('name', '!=', 'non_user_setting');
        })->where('status', 1)->get();

        $settings->where('name', 'show_me')->first()->default_value = $user->gender_search;
        $showAgesSetting = $settings->where('name', 'show_ages')->first()->default_value;

        /*
        |---------------------------------------------------------------------------------------
        | Calculate Age Value Range
        |---------------------------------------------------------------------------------------
        */

        if ($user->birthday) {
            $age = $user->birthday->age;
            $min_age = $age - 5;
            $max_age = $age + 5;
            $default_value = explode("-", $showAgesSetting);
            $min_age_max = $default_value[3] - 15;
            if ($age < 23) {
                $ageValues = $default_value[0].'-'.$default_value[0].'-'.$max_age.'-'.$default_value[3];
            }
            else {
                if ($age < ($default_value[3]-4)) {
                    $ageValues = $default_value[0].'-'.$min_age.'-'.$max_age.'-'.$default_value[3];
                }
                else {
                    $ageValues = $default_value[0].'-'.$min_age_max.'-'.$default_value[3].'-'.$default_value[3];
                }
            }
        }

        /*
        |---------------------------------------------------------------------------------------
        | Set Structure Of Settings To Save It Into The Database
        |---------------------------------------------------------------------------------------
        */

        foreach ($settings as $item => $value) {
            $data[$value->setting_id] = [
                'value' => $value->default_value
            ];
        }

        $user->settings()->sync($data);
    }
}
