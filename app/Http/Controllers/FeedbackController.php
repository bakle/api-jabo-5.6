<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\JaboEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller
{
    
    /**
     * Send user feed back
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->text
     * @return Json     
     **/
    public function sendFeedback(Request $request)
    {

        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric'            
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id With Pictures And Location , If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

       $user = User::with([
            'pictures' => function($query) {
                $query->bySize(500);
            },
            'location'            
        ])
        ->withCount('activityTypes')
        ->withCount('matches')
        ->findOrFail($request->user_id);


        /*
        |---------------------------------------------------------------------------------------
        | Send The Feedback Email 
        |---------------------------------------------------------------------------------------
        */
        
        Mail::send(new JaboEmails('feedback', $request, $user));

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();

    }
}
