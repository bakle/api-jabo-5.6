<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HealthInfoController extends Controller
{
    
    /**
     * Insert Or Update Health Info Data Of a
     * User.
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->array_keys. Health Info Names
     * @param string $request->array_values. Health Info Values.
     * @return Json     
     **/
    public function setHealthInfo(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'array_keys' => 'required',
            'array_values' => 'required',
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Get Health Info Names And Values. We Need To Convert The Values Into An Array And
        | Remove All '\\' Left In The Strings.
        |---------------------------------------------------------------------------------------
        */

        $health_names = str_replace("\\", "", explode("\\n", $request->array_keys));
        $health_Values = str_replace("\\", "", explode("\\n", $request->array_values));

        /*
        |---------------------------------------------------------------------------------------
        | Check If Both Arrays Have The Same Lenght, If Not, Then Throw Fail Operation 
        |---------------------------------------------------------------------------------------
        */

        if (count($health_names) != count($health_Values)) {
            return respondFailedOperation();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Loop The Array To Update Or Insert The Values. Also Set The $update_user Variable (Flag)
        | To False. The $update_user Variable Is a Flag To Know If The User Has A New "Weight" 
        | Or "Height" Value. This Flag Helps To Avoid Unnecessary Updates To The User Table.
        |---------------------------------------------------------------------------------------
        */

        $update_user = false;
        for ($i = 0; $i < count($health_names); $i++) {

            /*
            |---------------------------------------------------------------------------------------
            | If The Health Name Is "Weight" Or "Height", Then Add The Values To The User Table Too. 
            |---------------------------------------------------------------------------------------
            */
            
            if ($health_names[$i] == "weight") {
                $user->weight_value = $health_Values[$i];
                $update_user = true;
            }

            if ($health_names[$i] == "height") {
                $user->height_value = $health_Values[$i];
                $update_user = true;
            }

            /*
            |---------------------------------------------------------------------------------------
            | Get Health Data Of The User By Key 
            |---------------------------------------------------------------------------------------
            */

            $health_data = $user->healthData()->where('health_key', $health_names[$i])->first();

            /*
            |---------------------------------------------------------------------------------------
            | If Health Data Doesn't Exists, Then Create It, If Does Exists, Then Update It.
            |---------------------------------------------------------------------------------------
            */

            if (!$health_data) {
                $user->healthData()->create([
                    'health_key' => $health_names[$i],
                    'value' => $health_Values[$i],
                ]);
            }
            else {
                $user->healthData()->where('health_key', $health_names[$i])->update([                    
                    'value' => $health_Values[$i],
                ]);
            }

        }

        /*
        |---------------------------------------------------------------------------------------
        | If $update_user Variable Is True, Then Update The User Table With The New Weight Or
        | Height Values. 
        |---------------------------------------------------------------------------------------
        */

        if ($update_user) {
            $user->save();
        }        

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();

    }


    /**
     * Get Health Info Data Of A User.
     *
     * @author Bashir A.
     *
     * @param integer $request->user_id
     * @param string $request->health_key. Health Info Names     
     * @return Json     
     **/
    public function getHealthInfo(Request $request)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate Params
        |---------------------------------------------------------------------------------------
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'health_key' => 'required',            
        ]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

        if ($validator->fails()) {
            return respondIncorrectParameters();
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get User By Id, If Doesn't Exists, Throw Fail Error.
        |---------------------------------------------------------------------------------------
        */

        $user = User::findOrFail($request->user_id);

        /*
        |---------------------------------------------------------------------------------------
        | Get Health Info Names. We Need To Convert The Values Into An Array And
        | Remove All '\\' Left In The Strings.
        |---------------------------------------------------------------------------------------
        */

        $health_keys = str_replace("\\", "", explode("\\n", $request->health_key));

        /*
        |---------------------------------------------------------------------------------------
        | Get Health Info Of The User By The Health Keys 
        |---------------------------------------------------------------------------------------
        */

        $health_data = $user->healthData()->select('user_id', 'health_key', 'value')->whereIn('health_key', $health_keys)->get();

        if (count($health_data) > 0) {

            $health_data->transform(function($item) {
                $data['user_id'] = $item->user_id;
                $data['health_key'] = $item->health_key;
                $data['value'] = $item->value;

                return $data;
            });

            /*
            |---------------------------------------------------------------------------------------
            | Return Succesfully Status.
            |---------------------------------------------------------------------------------------
            */

            return respondSuccessfully($health_data);
        }
        
        /*
        |---------------------------------------------------------------------------------------
        | Return Results Not Found.
        |---------------------------------------------------------------------------------------
        */

        return respondResultsNotFound();

    }
}
