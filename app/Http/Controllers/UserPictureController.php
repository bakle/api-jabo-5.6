<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserPictureController extends Controller
{
    

    /**
	 * Get Pictures Profiles Of A User
	 *
	 *
	 * @param integer $request->user_id
	 * @param integer $request->index
	 * @param integer $request->total
	 * @return type
	 * @throws conditon
	 **/
	public function movePicture(Request $request)
	{
		
		/*
    	|---------------------------------------------------------------------------------------
    	| Validate Params
    	|---------------------------------------------------------------------------------------
        */
        
        $validator = Validator::make($request->all(), [
    		'user_id' => 'required|min:1|numeric',
    		'picture_ids' => 'required',
    	]);

        /*
        |---------------------------------------------------------------------------------------
        | If Validation Fails, The Return Incorrect Parameters Status 
        |---------------------------------------------------------------------------------------
        */

    	if ($validator->fails()) {
    		return respondIncorrectParameters();
		}		

		/*
		|---------------------------------------------------------------------------------------
		| Get User, If They Doesn't Existsm Then Throw Fail Error 
		|---------------------------------------------------------------------------------------
		*/

		$user = User::findOrFail($request->user_id);
        
        /*
        |---------------------------------------------------------------------------------------
        | Convert Picture Ids To Array From String Sepparated By Comma (,) 
        |---------------------------------------------------------------------------------------
        */

        $picture_ids = explode(',', $request->picture_ids);

        /*
        |---------------------------------------------------------------------------------------
        | Validate That The Number Of Pictures Id Are The Same Number Of Pictures Of The DB.
        | We Divide It By 3 Beacuse We Store 3 Record Of Each Picture
        |---------------------------------------------------------------------------------------
        */

        if (count($picture_ids) != ($user->pictures()->count() / 3)) {
            return respondFailedOperation();
        }
        

        $position = 1;

        /*
        |---------------------------------------------------------------------------------------
        | Loop Through Each Picture Id And Reposition The Pictures According The Id.
        | First We Get The Name Of The Picture And Then Update The Position Of All Pictures
        | With The Same Name. 
        |---------------------------------------------------------------------------------------
        */

        foreach ($picture_ids as $picture_id) {
            $picture = $user->pictures()->where('picture_id', $picture_id)->first();
            $pictureName = substr($picture->getOriginal('image_url'), 0,strpos($picture->getOriginal('image_url'), '.'));
            $pictureName = str_replace("_500", '', $pictureName);
            $pictureName = str_replace("_100", '', $pictureName);
            
            $user->pictures()->where('image_url', 'LIKE', '%' . $pictureName .'%')->update([
                'position'=> $position
            ]);

            $position++;
        }

        /*
        |---------------------------------------------------------------------------------------
        | Return Succesfully Status.
        |---------------------------------------------------------------------------------------
        */

        return respondSuccessfully();
        
    }
}
