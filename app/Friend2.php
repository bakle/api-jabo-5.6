<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend2 extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'user_id';


    protected $fillable = ['friend_id', 'status'];


    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "user_friends_2";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
