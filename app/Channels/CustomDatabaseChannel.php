<?php

namespace App\Channels;

use App\Notification as JaboNotification;
use Illuminate\Notifications\Notification;
use App\Exceptions\PushNotificationsException;

/**
 * Send Push Notifications To Android
 * Or IOS
 */
class CustomDatabaseChannel
{
    
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toCustomDatabase($notifiable);

        try {
            $notification = JaboNotification::create($message);
            session(['notification' => $notification]);
        }
        catch(\Exception $e) {
            throw new PushNotificationsException($e);
        }
    }
}

