<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use Edujugon\PushNotification\PushNotification;

/**
 * Send Push Notifications To Android
 * Or IOS
 */
class PushNotificationChannel
{
    
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toPushNotification($notifiable);

        $androidMessage = [
            'notification' => [
                'body' => $message['body'],
                'sound' => 'default',
                'badge' => $message['badge']
            ],
            'data' => 
                $message['custom']            
        ];

        $iosMessage = [
            'aps' => [
                'alert' => [
                    'body' => $message['body']
                ],
                'sound' => $message['sound'],
                'badge' => $message['badge']
            ],
            'custom' => $message['custom']
        ];        

        $user_active_sessions = $notifiable->activeSessions;
        
        if (count($notifiable->activeSessions) > 0) {
            $android_tokens = $notifiable->activeSessions->where('platform', 'android')->pluck('notification_token')->toArray();
            $ios_tokens = $notifiable->activeSessions->where('platform', 'iOs')->pluck('notification_token')->toArray();
            
            if (count($android_tokens) > 0) {                
                $push = new PushNotification('fcm');
                $response = $push->setMessage($androidMessage)->setDevicesToken($android_tokens)->send()->getFeedback();
                dd($response);
            }

            if (count($ios_tokens)) {
                $push = new PushNotification('apn');
                $response = $push->setMessage($iosMessage)->setDevicesToken($ios_tokens)->send()->getFeedback();
                dd($response);
            }            
        }
    }
}

