<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'notification_type_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "notification_type";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
