<?php

namespace app\Classes;

use Intervention\Image\Facades\Image;

class ImageProcess
{

    /**
	 * Transform and save image
	 *
     *  @author Bashir A.
     * 
	 * @param String $imagePath Path of original image.
     * @param String $filename Name of the image to transform.
     * @param String $imageExtension Type (Extension) of the image to transform.
	 * @param String $size Size to resize the original image.
	 
	 * @return void
	 * @throws conditon
	 **/
	public static function transformAndSave($imagePath, $filename, $imageExtension, $size)
	{
		
		/*
		|---------------------------------------------------------------------------------------
		| Build The Image Name According Size And Original Image
		|---------------------------------------------------------------------------------------
		*/

		$originalImageName = $imagePath . "/" . $filename . "." . $imageExtension;
		$destinationImageName = $imagePath . "/" . $filename . "_" . $size . "." . $imageExtension;

		/*
		|---------------------------------------------------------------------------------------
		| Instantiate (Make) Image From Path
		|---------------------------------------------------------------------------------------
		*/

		$image = Image::make($originalImageName);

		/*
		|---------------------------------------------------------------------------------------
		|  Resize Width Or Height Depending Of Size Of Image
		|---------------------------------------------------------------------------------------
		*/

		if ($image->width() > $image->height()) {
			$image->resize($size, null, function($constraint){
				$constraint->aspectRatio();
			});
		}
		else {
			$image->resize(null, $size, function($constraint){
				$constraint->aspectRatio();
			});
		}

		/*
		|---------------------------------------------------------------------------------------
		| Save Image 
		|---------------------------------------------------------------------------------------
		*/

		$image->save($destinationImageName);

		/*
		|---------------------------------------------------------------------------------------
		| To Blur Or Not To Blur ? If Ratio Is Less Than 0.95 Or Greater Than 1.05. Then Blur
		|---------------------------------------------------------------------------------------
		*/

		if ($image->width() / $image->height() > 1.05 || $image->width() / $image->height() < 0.95) {
			
			self::blurImage($imagePath, $filename, $imageExtension, $size);
		}
			
	}


	/**
	 * Blur Image
	 *
	 *
	 * @param Type $var Description
	 * @return type
	 * @throws conditon
	 **/
	public static function blurImage($basePath, $filename, $imageExtension, $size)
	{
		
		/*
		|---------------------------------------------------------------------------------------
		| Build The Image Name According Size And Original Image
		|---------------------------------------------------------------------------------------
		*/

		$originalImageName = $basePath . "/" . $filename . "_" . $size . "." . $imageExtension;
		$destinationImageName = $basePath . "/" . $filename . "_" . $size . "_blur" . "." . $imageExtension;

		/*
		|---------------------------------------------------------------------------------------
		| Get Image To Blur And Original Sized Image 
		|---------------------------------------------------------------------------------------
		*/

		$imageToBlur = new \Imagick($originalImageName);			
		$originalImage = new \Imagick($originalImageName);

		// Get Max Area to Expand
		$maxArea = max($imageToBlur->getImageWidth(), $imageToBlur->getImageHeight());

		/*
		|---------------------------------------------------------------------------------------
		| Blur Image And Resized To Max Area 
		|---------------------------------------------------------------------------------------
		*/

		$imageToBlur->gaussianBlurImage(22, 25);
		$imageToBlur->resizeImage($maxArea, $maxArea, \Imagick::DISPOSE_NONE, -1);

		/*
		|---------------------------------------------------------------------------------------
		|  Calculate The Position To Insert The Original Image Over The Blurred Image
		|---------------------------------------------------------------------------------------
		*/
			
		if ($originalImage->getImageWidth() < $originalImage->getImageHeight()) {

			// Calculate Center
			$center = ($maxArea / 2) - ($originalImage->getImageWidth() / 2);

			// Combine The Blurred Image And The Original Image
			$imageToBlur->compositeImage($originalImage, \Imagick::COMPOSITE_DEFAULT, $center, 0);
		}
		else {
				
			// Calculate Center
			$center = ($maxArea / 2) - ($originalImage->getImageHeight() / 2);

			// Combine The Blurred Image And The Original Image
			$imageToBlur->compositeImage($originalImage, \Imagick::COMPOSITE_DEFAULT, 0, $center);
		}
		
		$imageToBlur->writeImage($destinationImageName);
	}
}