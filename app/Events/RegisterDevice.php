<?php

namespace App\Events;

use App\user;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class RegisterDevice
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $request;
    public $user;


    /**
     * Create a new event instance.
     *
     * @author Bashir A.
     * @return void
     */
    public function __construct(Request $request, User $user)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
       // return new PrivateChannel('channel-name');
    }
}
