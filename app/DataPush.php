<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPush extends Model
{
    protected $guarded = [];


    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "data_push";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
