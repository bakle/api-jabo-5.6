<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'interest_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "user_interests";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | SCOPES
    |---------------------------------------------------------------------------------------
    */

    /**
     * This scope selects the common interest between a user and the given viewer.
     * 
     * @author Bashir A.
     *
     **/
    public function scopeCommonInterests($query,$viewer_id)
    {
        return $query->join("fitdate_user_interests AS fui",'fui.name',"=",'fitdate_user_interests.name')->where('fui.user_id',$viewer_id)->select('fitdate_user_interests.*')->distinct();
    }
}
