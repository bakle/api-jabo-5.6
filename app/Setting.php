<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'setting_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "setting";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | RELATIONSHIPS
    |---------------------------------------------------------------------------------------
    */

    /**
     * This relationship brings the user's settings.
     * 
     * @author Bashir A.
     *
     **/
    public function settingGroup()
    {
         return $this->belongsTo(SettingGroup::class,"setting_group_id");
    }
}
