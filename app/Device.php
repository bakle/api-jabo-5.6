<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'device_id';

    /**
     * Define the fields that can be mass assigned
     *
     * @var array
     **/
    protected $fillable = ['longitude', 'latitude'];


    /*
    |---------------------------------------------------------------------------------------
    | MUTATORS 
    |---------------------------------------------------------------------------------------
    */

    public function getNotificationTokenAttribute($value)
    {
        $token = str_replace("<","",$value);
        $token = str_replace(">","",$token);
        return $token;
    }
}
