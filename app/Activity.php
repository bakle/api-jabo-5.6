<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'activity_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "activity";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
