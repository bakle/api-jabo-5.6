<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Swipe extends Model
{
    
    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "swipe";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
