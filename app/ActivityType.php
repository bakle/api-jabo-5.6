<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'activity_type_id';


    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "activity_type";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | SCOPES
    |---------------------------------------------------------------------------------------
    */

    /**
     * This scope selects Keys, Values, User_id and Created_at date from health_data table.
     * The Values are converted according to the $distanceIn variable.
     * 
     * @author Bashir A.
     *
     **/
    public function scopeCommonActivites($query,$user_id)
    {
        return $query->join(env('DB_API_PREFIX') . "user_activity_type AS fuat", env('DB_API_PREFIX') . 'user_activity_type.activity_type_id',"=",'fuat.activity_type_id')->where('fuat.user_id',$user_id);
    }


    /*
    |---------------------------------------------------------------------------------------
    | ACCESSORS
    |---------------------------------------------------------------------------------------
    */

    /**
     * This accessor get the full url to get an activity icon image
     * 
     * @author Bashir A.
     *
     **/
    public function getIconUrlAttribute($value)
    {
        return url("api/resources/activities",$this->icon_name);
    }
}
