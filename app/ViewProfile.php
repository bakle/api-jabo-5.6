<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewProfile extends Model
{
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'view_id';

    /**
     * Define wich fields are protected for mass assignment.
     *
     * @var string
     **/
    protected $guarded = [];

    /**
     * Define if it will save the created_at and updated_at in the table.
     *
     * @var string
     **/
    public $timestamps = false;

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "user_view_profile";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}
