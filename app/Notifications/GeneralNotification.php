<?php

namespace App\Notifications;

use App\NotificationType;
use Illuminate\Bus\Queueable;
use App\Channels\CustomDatabaseChannel;
use App\Channels\PushNotificationChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Exceptions\PushNotificationsException;
use Illuminate\Notifications\Messages\MailMessage;

class GeneralNotification extends Notification
{
    use Queueable;
    
    private $notificationType;
    private $sender;
    private $notification;
    private $deal;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificationTypeId, $sender, $deal = null, $notification = null)
    {
        $this->notificationTypeId = $notificationTypeId;
        $this->sender = $sender;
        $this->notification = $notification;
        $this->deal = $deal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        /*
        |---------------------------------------------------------------------------------------
        | If Notification Exists (This Means That It Already Exists In The Database), Then
        | Don't Store It In The Database
        |---------------------------------------------------------------------------------------
        */

        if ($this->notification != null) {
            return [PushNotificationChannel::class];
        }

        return [CustomDatabaseChannel::class, PushNotificationChannel::class];
        
    }    

    /**
     * Get the push notification representation of 
     * the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toPushNotification($notifiable)
    {
        /*
        |---------------------------------------------------------------------------------------
        | Validate If User Can Receive Push Notification
        |---------------------------------------------------------------------------------------
        */

        if (!$this->validate($notifiable)) {
            return false;
        }

        /*
        |---------------------------------------------------------------------------------------
        | Chek If User Has Active Sessions, If Not, Then We Don't Send Push Notifications 
        |---------------------------------------------------------------------------------------
        */

        if (!count($notifiable->activeSessions)) {
            return false;
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get Notification Model 
        |---------------------------------------------------------------------------------------
        */

        $notification = $this->notification;
        if (!$this->notification) {
            $notification = session('notification');
        }

        /*
        |---------------------------------------------------------------------------------------
        | Get Date Of Match Record
        |---------------------------------------------------------------------------------------
        */

        $match_date = (string) optional($notifiable->matches()->where('match_user_id', $this->sender->user_id)->first())->created_at;
        
        /*
        |---------------------------------------------------------------------------------------
        | Set The Notification Text 
        |---------------------------------------------------------------------------------------
        */

        $notification_text = $notification->transformNotificationMessage($this->sender->first_name);

        /*
        |---------------------------------------------------------------------------------------
        | Get Badges Number 
        |---------------------------------------------------------------------------------------
        */

        $badges = $notifiable->notifications()->pending()->groupBy('item_id','notification_type','viewer_user_id')->count();

        /*
        |---------------------------------------------------------------------------------------
        | Get Sender Picture Profile 
        |---------------------------------------------------------------------------------------
        */

        $senderPictureProfile = (string) optional($this->sender->pictures()->bySize(100)->first())->image_url;
        
        return [
            'badge' => $badges,
            'sound' => 'bingbong.aiff',
            'body' => $notification_text,
            'custom' => [
                'custom data' => [
                    'notification_id' => (string) $notification->notification_id,
                    'notification_type' => (string) $notification->notification_type,
                    'user_id' => (string) $this->sender->user_id,
                    'first_name' => (string) $this->sender->first_name,
                    'last_name' => (string) $this->sender->last_name,
                    'gender' => (string) $this->sender->gender,
                    'image_url' => $senderPictureProfile,
                    'date_match' => $match_date,
                    'destination' => (string) $notification->notificationType->notification_destination_id,
                    'deal_id' => (string) optional($this->deal)->id,
                    'description' => (string) $notification->notificationType->description,
                ]
            ]
        ];
    }

    /**
     * Get the database representation of 
     * the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toCustomDatabase($notifiable)
    {        
        /*
		|---------------------------------------------------------------------------------------
		| Get Notification Type Or Fail
		|---------------------------------------------------------------------------------------
		*/
			
		$notification_type = NotificationType::find($this->notificationTypeId);
        
        if (!$notification_type) {
            throw new PushNotificationsException('Notification Type ID : ' . $this->notificationTypeId . ' Not Found. [' . now() .']');
        }

        return [
            'notification_type' => $notification_type->notification_type_id, 
            'tittle' => $notification_type->message,
            'text' => $notification_type->message,
            'viewed_status' => 0,
            'viewer_user_id' => $notifiable->user_id,
            'item_id' => $this->sender->user_id
        ];
    }


    /**
     * Validate if user can receive the push
     * notification
     *
     * @param mixed $notifiable
     * @return type
     * @throws conditon
     **/
    private function validate($notifiable)
    {
        $isLogged = $notifiable->logged;
        $hasAvailableAccount = $notifiable->status == 1 ? 1 : 0;
        $canReceivePushNotification = (int) optional(optional($notifiable->settings->where('setting_id', 16)->first())->pivot)->value;
        $isOnline = (int) !$notifiable->idle;
        $isInChat = (int) optional($notifiable->matches()->where('match_user_id', $this->sender->user_id)->first())->in_chat;

        /*
        |---------------------------------------------------------------------------------------
        | If Pass Validation, Then Return True, If Not, Return False 
        |---------------------------------------------------------------------------------------
        */

        if ($isLogged && $hasAvailableAccount && $canReceivePushNotification && !$isOnline && !$isInChat) {
            return true;
        }

        return false;
    }
}
