<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    
    protected $primaryKey = 'match_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "match_users";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


    /*
    |---------------------------------------------------------------------------------------
    | RELATIONSHIPS 
    |---------------------------------------------------------------------------------------
    */

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'item_id', 'match_user_id');
    }
}
