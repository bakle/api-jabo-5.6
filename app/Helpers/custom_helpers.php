<?php

/**
* Format the weight according to the meassurement unit (kg or lb).
*
* @author Bashir A.
* @param  string  $weightIn
* @param  numeric  $weight
* @return  string
**/
function format_weight_in($weightIn, $weight)
{
    switch ($weightIn) {
        case 'lb':
            $weight = round($weight*0.0022046);
            return strtoupper($weight." ".$weightIn.'s');
        break;
        case 'kg':
            $weight = round($weight/1000);
            return strtoupper($weight." ".$weightIn.'s');
        break;
    }
    return "";
}

/**
* Format the height according to the meassurement unit (cm or ft).
*
* @author Bashir A.
* @param  string  $heightIn
* @param  numeric  $weight
* @return  string
**/
function format_height_in($heightIn, $height)
{
    switch ($heightIn) {
        case 'cm':
            return strtoupper($height." ".$heightIn);
        break;
        case 'ft':
            $height_ft = floor($height/30.48);
            $height_in = round(($height/30.48 - $height_ft)*12);
            if ($height_in > 0) {
                return $height_ft."'".$height_in;
            }
            return $height_ft."'";
        break;
    }
    return "";
}

/**
* Transform activities with default values.
*
* @author Bashir A.
* @param  array|collection  $activities
* @param  boolean  $status
* @param  collection  $data
**/
function transform_activities($activities, $status = null)
{
    return $activities->transform(function ($item, $value) use ($status) {
        if($status){
            $data['status'] = $status;
        }
        $data['activity_type_id'] = (string) $item->pivot->activity_type_id;
        $data['name'] = (string) $item->name;
        $data['icon_name'] = (string) $item->icon_url;

        return $data;
    });    
}

/**
* Transform pictures with default values.
*
* @author Bashir A.
* @param  array|collection  $pictures
* @return collection  $data
**/
function transform_pictures($pictures)
{
    if (count($pictures) > 0) {
        $pictures->transform(function ($item, $value) {
            $data['picture_id'] = (string) $item->picture_id;
            $data['image_url'] = (string) $item->image_url;
            $data['position'] = (string) $item->position;
            return $data;
        });
    }

    return $pictures;
        
}

/**
* Transform pictures with default values.
*
* @author Bashir A.
* @param  array|collection  $friends
* @param  string  $level
* @return collection  $data
**/
function transform_friends($friends, $level)
{
    
    if (count($friends) > 0) {        
        return $friends->transform(function ($item, $value) use ($level) {

            if($item->user){
                $data['first_name'] = (string) $item->user->first_name;
                $data['last_name'] = (string) $item->user->last_name;
                $data['image_url'] = (string) optional($item->user->pictures)->first()->image_url;
                $data['level'] = (string) $level;

                return $data;
            }        
        });
    }

    return [];
    
}

/**
* Get Distance between 2 coordinates.
*
* @author Bashir A.
* @param  string   $distanceIn
* @param  numeric  $firstLatitude
* @param  numeric  $firstLongitude
* @param  numeric  $secondLatitude
* @param  numeric  $secondLongitude
* @return integer  $distance
**/
function get_distance($distanceIn, $firstLatitude, $firstLongitude, $secondLatitude, $secondLongitude)
{
    if (!$firstLatitude || !$firstLongitude || !$secondLatitude || !$secondLongitude) {
        return (int) "0";
    }

    if (!is_numeric($firstLatitude) || !is_numeric($firstLongitude) || !is_numeric($secondLatitude) || !is_numeric($secondLongitude)) {
        return (int) "0";
    }

    if ($distanceIn == 'km') {
        $distance = floor((acos(sin(deg2rad((float)$firstLatitude)) * sin(deg2rad((float)$secondLatitude)) + 
            cos(deg2rad((float)$firstLatitude)) * cos(deg2rad((float)$secondLatitude)) * 
            cos(deg2rad((float)$firstLongitude) - deg2rad((float)$secondLongitude))) * 3959)/0.62137);
    }
    else {
        $distance = floor(acos(sin(deg2rad((float)$firstLatitude)) * sin(deg2rad((float)$secondLatitude)) + 
            cos(deg2rad((float)$firstLatitude)) * cos(deg2rad((float)$secondLatitude)) * 
            cos(deg2rad((float)$firstLongitude) - deg2rad((float)$secondLongitude))) * 3959);
    }
        
    return (int) $distance;    
}

/**
* Transform health data with default values.
* @author Bashir A.
*
* @param  array|collection  $health_data
* @param  string  $setting_show_weight_in
* @param  string  $setting_show_height_in
* @param  string  $setting_show_distance_in
**/
function transform_health_data($health_data, $settings)
{
    /*
    |---------------------------------------------------------------------------------------
    | Get Settings
    |---------------------------------------------------------------------------------------
    */

    $showHeightIn = $settings->where("name", "show_height_in")->first()->pivot->value;
    $showWeightIn = $settings->where("name", "show_weight_in")->first()->pivot->value;
    $showDistanceIn = $settings->where("name", "show_distance_in")->first()->pivot->value;
    $showWeight = optional(optional($settings->where("name", "show_weight")->first())->pivot)->value;
    $showHeight = optional(optional($settings->where("name", "show_height")->first())->pivot)->value;

    /*
    |---------------------------------------------------------------------------------------
    | If There Are Heath Data, Then Assigns Values, If Not, Assign Empty Values
    |---------------------------------------------------------------------------------------
    */

    if (count($health_data)) {

        $healthKeysArray = array();
        foreach ($health_data as $key => $value) {
            $healthKeysArray[$value->health_key] = (string) $value->value;
        }
         
        if (!array_key_exists("weight",$healthKeysArray)) {
            $healthKeysArray["weight"] = 0;
        }        

        if (!array_key_exists("height",$healthKeysArray)) {
            $healthKeysArray["height"] = 0;
        }        

        if (!array_key_exists("avg_distance_walking",$healthKeysArray)) {
            $healthKeysArray["avg_distance_walking"] = "0";
        }

        /*
        |---------------------------------------------------------------------------------------
        | Convert Weight Value According Weight Setting
        |---------------------------------------------------------------------------------------
        */

        if ($showWeight == "1") {
            switch ($showWeightIn) {
                case 'lb':                
                    $healthKeysArray["weight"] = (string) round($healthKeysArray["weight"]*0.0022046);                
                break;
                case 'kg':
                    $healthKeysArray["weight"] = (string) round($healthKeysArray["weight"]/1000);                    
                break;
            }
        }
        else {
            $healthKeysArray["weight"] = "0";
        }

        /*
        |---------------------------------------------------------------------------------------
        | Convert Height Value According Height Setting
        |---------------------------------------------------------------------------------------
        */

        if ($showHeight == "1") {
            switch ($showHeightIn) {
                case 'ft':
                    $heightFt = (string) floor($healthKeysArray["height"]/30.48);
                    $heightIn = (string) round(($healthKeysArray["height"]/30.48 - $heightFt)*12);                    
                break;
            }
        }
        else {
            $healthKeysArray["height"] = "0";
        }
    }
    else {
        return $healthKeysArray = [
            "avg_steps_walking" => "0",
            "avg_distance_walking" => $showDistanceIn.":0",
            "avg_calories_burned" => "0",
            "weight" => $showWeightIn.":0",
            "height" => $showHeightIn.":0"
        ];
    }

    $data['avg_steps_walking'] = isset($healthKeysArray['avg_steps_walking']) ? $healthKeysArray['avg_steps_walking'] : "0";

    $data['avg_distance_walking'] = isset($healthKeysArray['avg_distance_walking']) ? $showDistanceIn.":".$healthKeysArray['avg_distance_walking'] : $showDistanceIn.":0";

    $data['avg_calories_burned'] = isset($healthKeysArray['avg_calories_burned']) ? $healthKeysArray['avg_calories_burned'] : "0";

    $data['weight'] = isset($healthKeysArray['weight']) ? $showWeightIn.":".$healthKeysArray['weight'] : $showHeightIn.":0";

    if (isset($heightIn)) {

        if ($heightIn > 0) {
            $height = $showHeightIn.":".$heightFt.",in:".$heightIn;
        }
        else {
            $height = $showHeightIn.":".$heightFt;
        }

        $data['height'] = (string) $height;
    }
    else {
        $data['height'] = isset($healthKeysArray['height']) ? $showHeightIn.":".$healthKeysArray['height'] : $showHeightIn.":0";
    }
    
    return $data;
}