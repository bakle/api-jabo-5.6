<?php

use App\Classes\userBlockedException;
use App\Exceptions\FailedOperationException;
use App\Exceptions\ResultsNotFoundException;
use App\Exceptions\IncorrectParametersException;

  /**
  * Transform the output to Json.
  * @author Bashir A.
  *
  * @param user_id, viewer_id
  **/
	function respond($status, $data = null)
	{
		/*
		|---------------------------------------------------------------------------------------
		| Define Response Output
		|---------------------------------------------------------------------------------------
		*/
		
		$response = [
			'status' => $status
		];
		if($data !== null){
        	$response['data'] = $data;        	
		}

		/*
		|---------------------------------------------------------------------------------------
		| Transform Response To Json
		|---------------------------------------------------------------------------------------
		*/
		
		return response()->json($response);		
	}

  
  /**
  * Set status to User Blocked.
  *
  * @author Bashir A.
  *
  * @return Status -4     
  **/
	function respondUserBlocked()
	{
    $status = [
			'code' => -4,
      'message' => "Error: User blocked"
		];

		return respond($status);
  }
    

  /**
  * Set status to Incorrect Parameters.
  *
  * @author Bashir A.
  *
  * @return Status -2
  **/
	function respondIncorrectParameters()
	{
    $status = [
			'code' => -2,
      'message' => "Error: incorrect parameters"
		];

		return respond($status);		
  }
    

  /**
  * Set status to Incorrect Parameters.
  *
  * @author Bashir A.
  *
  * @return Status -1
  **/
	function respondFailedOperation()
	{
    $status = [
			'code' => -1,
      'message' => "Error: Failed Operation"
		];

		return respond($status);		
  }
    

  /**
  * Set status to Result Not Found.
  *
  * @author Bashir A.
  *
  * @return Status 0
  **/
	function respondResultsNotFound()
	{
    $status = [
			'code' => 0,
      'message' => "Results not found"
		];

		return respond($status);    
  }


  /**
  * Set status to Succesfully
  *
  * @author Bashir A.
  *
  * @return Status 1
  **/
	function respondSuccessfully($data = null)
	{
		$status = [
			'code' => 1,
			'message' => "Successfully"
		];

		return respond($status, $data);
	}