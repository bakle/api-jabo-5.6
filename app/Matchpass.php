<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matchpass extends Model
{
    protected $primaryKey = 'match_pass_sender_id';

    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "match_pass_sender";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }
}