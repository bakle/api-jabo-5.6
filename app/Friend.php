<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
   
    /**
     * Define the primary key of the table related to the model.
     *
     * @var string
     **/
    protected $primaryKey = 'user_id';


    /**
     * Define the table related to the model.
     *
     * @var string
     **/
    public function __construct(array $attributes = [])
    {
        $table = env('DB_API_PREFIX') . "user_friends";
        $this->setTable($table);
        $this->bootIfNotBooted();
        $this->syncOriginal();
        $this->fill($attributes);
    }


	/*
    |---------------------------------------------------------------------------------------
    | RELATIONSHIPS
    |---------------------------------------------------------------------------------------
    */

    /**
     * This relationship gets the related user of the friend table.
     * 
     * @author Bashir A.
     *
     **/
    public function user()
    {
    	return $this->belongsTo(User::class, "friend_id","facebook_id");
    }


	/*
    |---------------------------------------------------------------------------------------
    | SCOPES
    |---------------------------------------------------------------------------------------
    */

    /**
     * This scope selects the common friends between a user and the given viewer.
     * 
     * @author Bashir A.
     *
     **/
    public function scopeCommonFriends($query,$viewer_facebook_id)
    {
        return $query->join("fitdate_user_friends AS fuf",'fuf.friend_id',"=",'fitdate_user_friends.friend_id')->where('fuf.user_id',$viewer_facebook_id)->select('fitdate_user_friends.*')->distinct();
    }

    /**
     * This scope selects the friends of friends of a user.
     * 
     * @author Bashir A.
     *
     **/
    public function scopeWithFriends($query)
    {
        return $query->with([
            "user", 
            "user.pictures" => function ($query) {
                $query->bySize('500');
            },
            "user.friends",
            "user.friends.user",
            "user.friends.user.pictures" => function ($query) {
                $query->bySize('500');
            }
        ]);
    }
}
